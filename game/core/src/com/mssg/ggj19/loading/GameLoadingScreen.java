package com.mssg.ggj19.loading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mssg.ggj19.GGJ19Main;
import com.mssg.ggj19.assets.AssetsLoader;

public class GameLoadingScreen implements Screen {

    ShapeRenderer shapeRenderer;

    public GameLoadingScreen() {
        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(0, 0,
                Gdx.graphics.getWidth() * AssetsLoader.getProgressPrc(), 20);
        shapeRenderer.end();

        if(AssetsLoader.isLoadingFinished()){
            GGJ19Main.THIS.finishLoading();
            GGJ19Main.THIS.goToMainMenuScreen();
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }
}
