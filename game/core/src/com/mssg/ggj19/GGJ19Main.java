package com.mssg.ggj19;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.mssg.ggj19.assets.AssetsLoader;
import com.mssg.ggj19.game.GameScreen;
import com.mssg.ggj19.game.entities.GameProps;
import com.mssg.ggj19.gameroom.setup.GameRoomScreen;
import com.mssg.ggj19.loading.GameLoadingScreen;
import com.mssg.ggj19.mainMenu.menu.MenuScreen;
import com.mssg.ggj19.network.NetworkController;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.PlayerLoadedEvent;

public class GGJ19Main extends Game {

    public static GGJ19Main THIS;
    private GameLoadingScreen gameLoadingScreen;

    private MenuScreen mainMenuScreen;
    private GameRoomScreen gameRoomScreen;
    private GameScreen gameScreen;

    public GGJ19Main() {
        THIS = this;
    }

    @Override
    public void create() {
        AssetsLoader.loadAllAssets();
        gameLoadingScreen = new GameLoadingScreen();
        setScreen(gameLoadingScreen);
        NetworkController.prepareNetworkStuff();
    }

    /**
     * Other screens are loaded separately, because assets need to load&initialize first
     */
    private void createAllOtherScreens() {
        mainMenuScreen = new MenuScreen();
        gameRoomScreen = new GameRoomScreen();
        gameScreen = new GameScreen();

        GameProps.preparePropProperties();
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetsLoader.disposeAllAssets();
    }

    public void goToMainMenuScreen() {
        setScreen(mainMenuScreen);
    }

    public void finishLoading() {
        AssetsLoader.initializeAssets();
        createAllOtherScreens();
    }

    public void goToGameScreen() {
        setScreen(gameScreen);
        GameClientController.get().getClient()
                .sendTCP(new PlayerLoadedEvent());
    }

    public void goToLobby() {
        setScreen(gameRoomScreen);
    }
}
