package com.mssg.ggj19.common.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.assets.AssetsProperties;
import com.mssg.ggj19.assets.AssetsUI;
import com.mssg.ggj19.gameroom.setup.GameRoomStage;
import com.mssg.ggj19.mainMenu.actions.GameMenuAction;

public class DialogWindow extends Dialog {

    private static final DialogWindow THIS = new DialogWindow();

    private Label questionLabel;
    private TextButton yesButton;
    private TextButton noButton;
    private TextButton okButton;
    private GameMenuAction action;

    private DialogWindow() {
        super(AssetsI18N.LANGUAGE.get("menuActions.confirmDialog.title"), AssetsUI.SHADE_SKIN);

        setSize(Integer.parseInt(AssetsProperties.CONFIG.getProperty("defaultDialogWindow.width")),
                Integer.parseInt(AssetsProperties.CONFIG.getProperty("defaultDialogWindow.height")));

        questionLabel = new Label("", AssetsUI.SHADE_SKIN);
        yesButton = new TextButton(AssetsI18N.LANGUAGE.get("generic.yes"), AssetsUI.SHADE_SKIN);
        noButton = new TextButton(AssetsI18N.LANGUAGE.get("generic.no"), AssetsUI.SHADE_SKIN);
        okButton = new TextButton("Ok", AssetsUI.SHADE_SKIN);
        getContentTable().add(questionLabel);

        row();

        addListeners();
    }

    private void addNoButton() {
        getButtonTable().add(noButton);
    }

    private void addYesButton() {
        getButtonTable().add(yesButton);
    }

    private void addOkButton() {
        getButtonTable().add(okButton);
    }

    public static Dialog getConfirmActionWindow(String question, GameMenuAction action) {

        THIS.getTitleLabel().setText("Confirm");
        THIS.getButtonTable().clear();
        THIS.questionLabel.setText(question);
        THIS.action = action;

        THIS.addYesButton();
        THIS.addNoButton();

        return THIS;
    }

    public static DialogWindow getErrorDialog(String error, GameMenuAction action) {
        THIS.getTitleLabel().setText("Error");
        THIS.getButtonTable().clear();
        THIS.questionLabel.setText(error);
        THIS.action = action;

        THIS.addOkButton();

        return THIS;
    }

    private void addListeners() {
        yesButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DialogWindow.this.remove();
                callAction();
            }
        });

        noButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                makeStageActorsTouchable();
                DialogWindow.this.remove();
            }

            private void makeStageActorsTouchable() {
                for (int i = 0; i < getStage().getActors().size; i++){
                    if(getStage().getActors().items[i].isVisible()) {
                        getStage().getActors().items[i].setTouchable(Touchable.enabled);
                    }
                }
            }
        });

        okButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                makeStageActorsTouchable();
                DialogWindow.this.remove();
            }

            private void makeStageActorsTouchable() {
                for (int i = 0; i < getStage().getActors().size; i++){
                    if(getStage().getActors().items[i].isVisible()) {
                        getStage().getActors().items[i].setTouchable(Touchable.enabled);
                    }
                }
            }
        });
    }

    private void callAction(){
        this.action.doAction();
        this.action = null;
    }
}
