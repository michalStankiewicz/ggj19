package com.mssg.ggj19.common.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.assets.AssetsUI;
import com.mssg.ggj19.mainMenu.actions.GameMenuAction;
import com.mssg.ggj19.mainMenu.menu.MenuStage;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.PlayerSendMessageEvent;

public class ChatView extends Table {

    private static ChatView THIS;
    private final Window playersListWindow;

    public static final ChatView get(){
        if(THIS == null) {
            THIS = new ChatView(AssetsUI.SHADE_SKIN);
        }
        return THIS;
    }

    private TextArea chatTextArea;
    private TextField chatTextBox;
    private TextButton sendMessageButton;
    private TextButton leaveGameButton;


    private ChatView(Skin skin) {
        super(skin);
//        setBackground(new SpriteDrawable(new Sprite(GraphicAssets.get().background)));

        setFillParent(true);
//        setDebug(true);

        chatTextArea = new TextArea("", skin);
        chatTextArea.setPrefRows(6);
        chatTextArea.setDisabled(true);

        chatTextBox = new TextField("", skin);
        chatTextBox.setMaxLength(200);

        sendMessageButton = new TextButton(AssetsI18N.LANGUAGE.get("chatView.sendMessageButton.text"), skin);
        leaveGameButton = new TextButton(AssetsI18N.LANGUAGE.get("chatView.quitGameButton.text"), skin);
        playersListWindow = new Window(AssetsI18N.LANGUAGE.get("chatView.playersList.title"), skin);

//        playersListWindow.setMovable(false);
//        add(playersListWindow).growY().width(200);
        add().colspan(3).expandY();//.grow();
        row();

        add(chatTextArea).colspan(3).expandX().fill();
        row();

        add(chatTextBox).expandX().fill();
        add(sendMessageButton);
        add(leaveGameButton);

        addListeners();
    }

    private void addListeners() {
        chatTextBox.setFocusTraversal(false);
        chatTextBox.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char key) {
                if ((key == '\r' || key == '\n')){
                    sendMessage(textField);
                }
            }
        });

        sendMessageButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(chatTextBox.getText().length() == 0){
                    return;
                }
                sendMessage(chatTextBox);
            }
        });

        leaveGameButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //am I host?

                if(GameServerController.get().amIHost()){
                    DialogWindow.getConfirmActionWindow(
                            "You are host. Are you sure?",
                            GameMenuAction.UNHOST_GAME_ACTION)
                        .show(getStage());
                } else{
                    DialogWindow.getConfirmActionWindow(
                            "Are you sure?",
                            GameMenuAction.LEAVE_GAME_ACTION)
                        .show(getStage());
                }
            }
        });
    }

    private void sendMessage(TextField textField) {
        PlayerSendMessageEvent messageEvent = new PlayerSendMessageEvent();
        messageEvent.setPlayer(GameClientController.get().getGameState().getMe());
        messageEvent.setMessage(textField.getText());
        GameClientController.get().getClient()
                .sendTCP(messageEvent);
        textField.setText("");
    }

    public void addChatMessage(String message){
        chatTextArea.appendText(message);
        chatTextArea.appendText("\n");
    }

    public void clearChat() {
        chatTextArea.setText("");
    }
}
