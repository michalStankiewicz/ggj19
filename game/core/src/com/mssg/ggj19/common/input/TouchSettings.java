package com.mssg.ggj19.common.input;

/**
 * Created by stonek on 8/27/16.
 */
public class TouchSettings {

    private static final TouchSettings THIS = new TouchSettings();

    private TouchProcessor touchProcessor;

    public static final TouchSettings get(){
        return THIS;
    }

    public void setTouchProcessor(TouchProcessor touchProcessor) {
        touchProcessor.reset();
        this.touchProcessor = touchProcessor;
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return touchProcessor.touchDown(screenX, screenY, pointer, button);
    }

    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return touchProcessor.touchUp(screenX, screenY, pointer, button);
    }

    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return touchProcessor.touchDragged(screenX, screenY, pointer);
    }
}
