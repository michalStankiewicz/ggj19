package com.mssg.ggj19.common.input;

/**
 * Created by stonek on 8/27/16.
 */
public interface TouchProcessor {
    boolean touchDown(int screenX, int screenY, int pointer, int button);
    boolean touchUp(int screenX, int screenY, int pointer, int button);
    boolean touchDragged(int screenX, int screenY, int pointer);

    void reset();
}
