package com.mssg.ggj19.common.input;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stonek on 5/15/16.
 */
public class ControlsSettings {
    private static final ControlsSettings THIS = new ControlsSettings();

    private final Map<String,Integer> keyMap;
    private final Map<String,Integer> mouseKeyMap;

    private ControlsSettings(){
        keyMap = new HashMap<>();
        mouseKeyMap = new HashMap<>();
    }

    public static ControlsSettings get(){
        return THIS;
    }

    public int getKeyCode(String option) {
        if(keyMap.containsKey(option)) {
            return keyMap.get(option);
        }
        else if(mouseKeyMap.containsKey(option)){
            return mouseKeyMap.get(option);
        }
        else{
            return -1;
        }
    }
// TODO planetdefender
//    public void setControls(Controls controls){
//        int keysControlsSize = controls._countKeys();
//        for(int i = 0; i < keysControlsSize; i++){
//            SingleKeyMapping skm = controls.keyMappings.get(i);
//            keyMap.put(skm.option,skm.key);
//        }
//
//        int mouseControlsSize = controls._countMouseKeys();
//        for(int i = 0; i <mouseControlsSize; i++){
//            SingleKeyMapping skm = controls.mouseKeyMappings.get(i);
//            mouseKeyMap.put(skm.option,skm.key);
//        }
//    }
}
