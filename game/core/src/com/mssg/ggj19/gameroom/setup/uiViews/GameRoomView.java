package com.mssg.ggj19.gameroom.setup.uiViews;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.common.ui.ChatView;
import com.mssg.ggj19.gameroom.setup.GameRoomStage;

public class GameRoomView extends Table {


    public Label waitingLabel;

    public GameRoomView(Skin skin, GameRoomStage gameSetupStage) {
        super(skin);
        setFillParent(true);

        waitingLabel = new Label("AAAA", skin, "title");

        add(waitingLabel).center();
    }

    public void updatePlayersCount(int playersCount) {
        waitingLabel.setText("Waiting for players... " + playersCount + "/6");
    }
}
