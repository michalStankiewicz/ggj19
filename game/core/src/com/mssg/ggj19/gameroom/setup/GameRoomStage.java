package com.mssg.ggj19.gameroom.setup;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mssg.ggj19.assets.AssetsUI;
import com.mssg.ggj19.common.ui.ChatView;
import com.mssg.ggj19.gameroom.setup.uiViews.GameRoomView;
import com.mssg.ggj19.mainMenu.menu.uiViews.OptionsView;
import com.mssg.ggj19.network.game.GameClientController;

public class GameRoomStage extends Stage {


    private GameRoomView gameRoomView;

    public GameRoomStage() {
        gameRoomView = new GameRoomView(AssetsUI.SHADE_SKIN, this);

    }

    public void show() {
        addActor(gameRoomView);
        addActor(ChatView.get());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        gameRoomView.updatePlayersCount(GameClientController.get()
                .getGameState()
                .getPlayers()
                .size());
    }
}
