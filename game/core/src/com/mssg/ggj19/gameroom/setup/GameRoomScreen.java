package com.mssg.ggj19.gameroom.setup;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class GameRoomScreen implements Screen {

    private static GameRoomScreen INSTANCE;

    private GameRoomStage gameRoomStage;

    public GameRoomScreen() {
        gameRoomStage = new GameRoomStage();
        INSTANCE = this;
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(gameRoomStage);
        gameRoomStage.show();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        gameRoomStage.act(delta);
        gameRoomStage.act();
        gameRoomStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gameRoomStage.getViewport().update(width, height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
    }
}