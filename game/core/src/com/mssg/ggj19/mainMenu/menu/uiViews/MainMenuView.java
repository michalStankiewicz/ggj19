package com.mssg.ggj19.mainMenu.menu.uiViews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.mainMenu.actions.GameMenuAction;
import com.mssg.ggj19.mainMenu.menu.MenuStage;

public class MainMenuView extends Table {

    private TextButton singlePlayerGameButton;
    private TextButton joinGameButton;
    private TextButton hostGameButton;
    private TextButton tutorialButton;
    private TextButton optionsButton;
    private TextButton creditsButton;
    private TextButton quitButton;


    public MainMenuView(Skin skin, final MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        joinGameButton = new TextButton(AssetsI18N.LANGUAGE.get("mainMenu.joinGameButton.text"), skin);
        hostGameButton = new TextButton(AssetsI18N.LANGUAGE.get("mainMenu.hostGameButton.text"), skin);
        singlePlayerGameButton = new TextButton(AssetsI18N.LANGUAGE.get("mainMenu.singlePlayerGameButton.text"), skin);
        optionsButton = new TextButton(AssetsI18N.LANGUAGE.get("mainMenu.optionsButton.text"), skin);
        quitButton = new TextButton(AssetsI18N.LANGUAGE.get("mainMenu.quitButton.text"), skin);
        tutorialButton = new TextButton(AssetsI18N.LANGUAGE.get("mainMenu.tutorialButton.text"), skin);
        creditsButton = new TextButton(AssetsI18N.LANGUAGE.get("mainMenu.creditsButton.text"), skin);

        add().width(30).expandY();
        add().expand();
        row();

//        addMenuButton(singlePlayerGameButton);
        addMenuButton(joinGameButton);
        addMenuButton(hostGameButton);
//        addMenuButton(tutorialButton);
//        addMenuButton(optionsButton);
        addMenuButton(creditsButton);
        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).expandX();

        //TODO single player mode
//        singlePlayerButton.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                SoundAssets.get().menuClick.play();
//                TODO
//                MenuScreen.getInstance().hostSinglePlayerGame();
//            }
//        });

        hostGameButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToHostGameView();
            }
        });

        joinGameButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToJoinGameView();
            }
        });

        tutorialButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToTutorialView();
            }
        });

        optionsButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToOptionsView();
            }
        });

        creditsButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToCreditsView();
            }
        });

        quitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.confirmAction("Are you sure you want to quit?",
                        GameMenuAction.QUIT_GAME_ACTION);
            }
        });
    }


    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).expandX().left();
        row();
    }
}
