package com.mssg.ggj19.mainMenu.menu.uiViews;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.mainMenu.menu.MenuStage;

public class OptionsView extends Table {

    private TextButton returnButton;
    private Label soundLabel;
    private CheckBox soundCheckbox;

    public OptionsView(Skin skin, MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        soundLabel = new Label(AssetsI18N.LANGUAGE.get("options.sound.text"), skin);
        soundCheckbox = new CheckBox(null, skin, "switch");

        returnButton = new TextButton(AssetsI18N.LANGUAGE.get("generic.back"), skin);

        buildUI();

        addListeners(menuStage);
    }

    private void buildUI() {
        add().width(30).expandY();
        add().colspan(2).expand();
        row();

        addField(soundLabel, soundCheckbox);

        add().width(30).height(100);
        add().height(100).colspan(2).expandX();
        row();

        addMenuButton(returnButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
    }

    private void addField(Label label, Actor field) {
        add().width(30);

        add(label).width(200).left();
        add(field).width(150).expandX().padLeft(10).left();

        row();
    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }

    private void addListeners(final MenuStage menuStage) {
        returnButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });
    }
}
