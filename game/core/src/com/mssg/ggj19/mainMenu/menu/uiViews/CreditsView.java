package com.mssg.ggj19.mainMenu.menu.uiViews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.assets.AssetsProperties;
import com.mssg.ggj19.assets.AssetsUI;
import com.mssg.ggj19.mainMenu.menu.MenuStage;

public class CreditsView extends Table {
    private TextButton quitButton;

    private MenuStage menuStage;
    private final Label creditsLabel;

    public CreditsView(Skin skin, final MenuStage menuStage) {
        super(skin);
        this.menuStage = menuStage;

        setFillParent(true);
        creditsLabel = new Label(AssetsProperties.CONFIG.getProperty("credits"), AssetsUI.SHADE_SKIN, "title");
        quitButton = new TextButton(AssetsI18N.LANGUAGE.get("generic.back"), skin);

        add().width(30).expandY();
        add(creditsLabel).colspan(2).expand();
        row();

        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }
}

