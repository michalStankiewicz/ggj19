package com.mssg.ggj19.mainMenu.menu;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.assets.AssetsUI;
import com.mssg.ggj19.common.ui.DialogWindow;
import com.mssg.ggj19.mainMenu.actions.GameMenuAction;
import com.mssg.ggj19.mainMenu.menu.uiViews.CreditsView;
import com.mssg.ggj19.mainMenu.menu.uiViews.HostGameView;
import com.mssg.ggj19.mainMenu.menu.uiViews.JoinGameView;
import com.mssg.ggj19.mainMenu.menu.uiViews.MainMenuView;
import com.mssg.ggj19.mainMenu.menu.uiViews.OptionsView;
import com.mssg.ggj19.mainMenu.menu.uiViews.TutorialView;

public class MenuStage extends Stage {

    private static MenuStage THIS;
    private MainMenuView mainMenuView;
    private JoinGameView joinGameView;
    private HostGameView hostGameView;
    private CreditsView creditsView;
    private TutorialView tutorialView;
    private OptionsView optionsView;

    public MenuStage() {
        THIS = this;
        mainMenuView = new MainMenuView(AssetsUI.SHADE_SKIN, this);
        joinGameView = new JoinGameView(AssetsUI.SHADE_SKIN, this);
        hostGameView = new HostGameView(AssetsUI.SHADE_SKIN, this);
        creditsView = new CreditsView(AssetsUI.SHADE_SKIN, this);
        tutorialView = new TutorialView(AssetsUI.SHADE_SKIN, this);
        optionsView = new OptionsView(AssetsUI.SHADE_SKIN, this);

        addActor(mainMenuView);
        addActor(joinGameView);
        addActor(hostGameView);
        addActor(creditsView);
        addActor(tutorialView);
        addActor(optionsView);

        switchToMainMenu();
    }

    public static MenuStage getInstance() {
        return THIS;
    }

    @Override
    public boolean keyDown(int keyCode) {
        if(Input.Keys.ESCAPE == keyCode && mainMenuView.isVisible()){
            confirmAction(AssetsI18N.LANGUAGE.get("menuActions.confirmExitGame.text"),
                    GameMenuAction.QUIT_GAME_ACTION);
        }
        return true;
    }

    public void switchToMainMenu() {
        switchTo(mainMenuView);
    }

    public void switchToHostGameView() {
        switchTo(hostGameView);
    }

    public void switchToJoinGameView() {
        switchTo(joinGameView);
    }

    public void switchToTutorialView() {
        switchTo(tutorialView);
    }

    public void switchToCreditsView() {
        switchTo(creditsView);
    }

    public void switchToOptionsView() {
        switchTo(optionsView);
    }

    private void hideAllViews() {
        for (int i = 0; i < getActors().size; i++){
            getActors().items[i].setVisible(false);
        }
    }

    public void showError(String error) {
        getActors().get(0).setTouchable(Touchable.disabled);
        DialogWindow.getErrorDialog(error, GameMenuAction.NOP_ACTION).show(this);
    }

    public void confirmAction(String question, GameMenuAction action) {
        getActors().get(0).setTouchable(Touchable.disabled);
        DialogWindow.getConfirmActionWindow(
                question, action)
                .show(this);
    }

    private void switchTo(Table menuView){
        hideAllViews();
        menuView.setTouchable(Touchable.enabled);
        menuView.setVisible(true);
    }
}
