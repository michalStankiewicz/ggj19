package com.mssg.ggj19.mainMenu.menu.uiViews;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.mainMenu.menu.MenuStage;
import com.mssg.ggj19.network.NetworkController;
import com.mssg.ggj19.network.connector.objects.ConnectionInitEvent;

public class HostGameView extends Table {

    private Label userNameLabel;
    private TextField userNameTextField;

    private Label passwordLabel;
    private TextField passwordField;

    private TextButton startGameButton;
    private TextButton returnButton;

    public HostGameView(Skin skin, MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        userNameLabel = new Label(AssetsI18N.LANGUAGE.get("multiplayer.username.text"), skin);
        userNameTextField = new TextField("", skin);
        passwordLabel = new Label(AssetsI18N.LANGUAGE.get("multiplayer.password.text"), skin);
        passwordField = new TextField("", skin);
        passwordField.setPasswordMode(true);
        passwordField.setPasswordCharacter('*');
        startGameButton = new TextButton(AssetsI18N.LANGUAGE.get("multiplayer.startButton.text"), skin);
        returnButton = new TextButton(AssetsI18N.LANGUAGE.get("generic.back"),skin);

        //TODO remove later
        userNameTextField.setText("PLAYER-01");


        buildUI();

        addListeners(menuStage);
        validateHostSettings();
    }

    private void buildUI() {
        add().width(30).expandY();
        add().colspan(2).expand();
        row();

        addField(userNameLabel, userNameTextField);
        addField(passwordLabel, passwordField);

        add().width(30).height(10);
        add().height(10).colspan(2).expandX();
        row();

        addMenuButton(startGameButton);
        addMenuButton(returnButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
    }

    private void addField(Label label, TextField field) {
        add().width(30);

        add(label).width(200).left();
        add(field).width(150).expandX().padLeft(10).left();

        row();
    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }

    private void addListeners(final MenuStage menuStage) {
        returnButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

        userNameTextField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateHostSettings();
            }
        });

        startGameButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                NetworkController.hostGame(passwordField.getText());
                ConnectionInitEvent connectionInitEvent = new ConnectionInitEvent(userNameTextField.getText(),
                        passwordField.getText(),
                        "127.0.0.1");
                NetworkController.joinLocal(connectionInitEvent);
                startGameButton.setTouchable(Touchable.disabled);
                startGameButton.addAction(new TemporalAction(3) {
                    @Override
                    protected void update(float percent) {

                    }

                    @Override
                    protected void end() {
                        actor.setTouchable(Touchable.enabled);
                        super.end();
                    }
                });
//                int port = NetworkController.hostGame();
//                NetworkController.startClient();
                //TODO host game
//                SoundAssets.get().menuClick.play();
//                connectionEstablishData.setPlayerName(userNameTextField.getText());
//                connectionEstablishData.setPasswordHashed(passwordField.getText());
//                MenuScreen.getInstance().hostGame(connectionEstablishData);
            }
        });
    }

    private void validateHostSettings() {
        if(userNameTextField.getText() == null ||
                userNameTextField.getText().equals("")){
            startGameButton.setTouchable(Touchable.disabled);
            startGameButton.setVisible(false);
            return;
        }

        startGameButton.setTouchable(Touchable.enabled);
        startGameButton.setVisible(true);
    }
}
