package com.mssg.ggj19.mainMenu.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mssg.ggj19.assets.AssetGraphics;

public class MenuScreen implements Screen {

    private static MenuScreen INSTANCE;
    private static boolean isActive;
    private final SpriteBatch batch;
    private final Sprite background;
    private final Camera camera;
    private final Viewport viewport;

    private MenuStage menuStage;
    private Sprite titlebg;

    public static MenuScreen getInstance(){
        if(isActive) {
            return INSTANCE;
        }
        else{
            throw new IllegalStateException();
        }
    }

    public MenuScreen() {
        menuStage = new MenuStage();
        MenuScreen.INSTANCE = this;


        batch = new SpriteBatch();
        background = new Sprite(AssetGraphics.BACKGROUND);
        background.setCenter(0,0);
        camera = new OrthographicCamera();
        viewport = new FillViewport(Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight(), camera);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(menuStage);
        isActive = true;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();
        background.draw(batch);
        batch.end();

        menuStage.act(delta);
        menuStage.act();
        menuStage.getViewport().apply();
        menuStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        menuStage.getViewport().update(width, height);
        viewport.update(width, height);
    }

    @Override
    public void pause() {
        isActive = false;
    }

    @Override
    public void resume() {
        isActive = true;
    }

    @Override
    public void hide() {
        isActive = false;
    }

    @Override
    public void dispose() {
        isActive = false;
    }

}
