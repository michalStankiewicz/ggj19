package com.mssg.ggj19.mainMenu.menu.uiViews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.mainMenu.menu.MenuStage;

public class TutorialView extends Table {

    private Image tutorial;
    private TextButton quitButton;


    public TutorialView(Skin skin, final MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        tutorial = new Image();
        //TODO tutorial screenshot
//        tutorial = new Image(GraphicAssets.get().tutorial);
//        tutorial.setSize(tutorial.getWidth() * 0.7f,
//                tutorial.getHeight() * 0.7f);
        quitButton = new TextButton(AssetsI18N.LANGUAGE.get("generic.back"), skin);

        add().width(30).expandY();
        add(tutorial).colspan(2).expand();
        row();

        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();

        quitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }
}
