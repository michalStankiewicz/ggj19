package com.mssg.ggj19.mainMenu.menu.uiViews;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.assets.AssetsI18N;
import com.mssg.ggj19.mainMenu.menu.MenuStage;
import com.mssg.ggj19.network.NetworkController;
import com.mssg.ggj19.network.connector.ConnectorClientController;
import com.mssg.ggj19.network.connector.objects.ConnectionInitEvent;

public class JoinGameView extends Table {

    private Label ipLabel;
    private TextField ipTextField;
    private Label portLabel;
    private TextField portTextField;

    private Label userNameLabel;
    private TextField userNameTextField;

    private Label passwordLabel;
    private TextField passwordField;

    private TextButton joinGameButton;

    private TextButton returnButton;

    public JoinGameView(Skin skin, MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        ipLabel = new Label(AssetsI18N.LANGUAGE.get("multiplayer.ip.text"), skin);
        ipTextField = new TextField("127.0.0.1", skin);
        portLabel = new Label(AssetsI18N.LANGUAGE.get("multiplayer.port.text"), skin);
        portTextField = new TextField("12345", skin);
        userNameLabel = new Label(AssetsI18N.LANGUAGE.get("multiplayer.username.text"), skin, "subtitle");
        userNameTextField = new TextField("", skin);
        passwordLabel = new Label(AssetsI18N.LANGUAGE.get("multiplayer.password.text"), skin, "subtitle");
        passwordField = new TextField("", skin);
        passwordField.setPasswordMode(true);
        passwordField.setPasswordCharacter('*');
        joinGameButton = new TextButton(AssetsI18N.LANGUAGE.get("multiplayer.joinButton.text"), skin);
        returnButton = new TextButton(AssetsI18N.LANGUAGE.get("generic.back"), skin);

        buildUI();

        addListeners(menuStage);
        validateJoinGameSettings();
    }

    private void buildUI() {
        add().width(30).expandY();
        add().colspan(2).expand();
        row();

        addField(ipLabel, ipTextField);
        addField(portLabel, portTextField);
        addField(userNameLabel, userNameTextField);
        addField(passwordLabel, passwordField);

        add().width(30).height(10);
        add().height(10).colspan(2).expandX();
        row();

        addMenuButton(joinGameButton);
        addMenuButton(returnButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
    }

    private void addField(Label label, TextField field) {
        add().width(30);

        add(label).width(200).left();
        add(field).width(150).expandX().padLeft(10).left();

        row();
    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }

    private void addListeners(final MenuStage menuStage) {
        userNameTextField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateJoinGameSettings();
            }
        });

        ipTextField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateJoinGameSettings();
            }
        });

        portTextField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateJoinGameSettings();
            }
        });

        returnButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

        joinGameButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ConnectorClientController.get().join(
                        ipTextField.getText(),
                        Integer.parseInt(portTextField.getText()),
                        new ConnectionInitEvent(
                                userNameTextField.getText(),
                                passwordField.getText(),
                                ipTextField.getText()));
            }
        });
    }

    private void validateJoinGameSettings() {
        if(userNameTextField.getText() == null ||
                userNameTextField.getText().equals("")){
            joinGameButton.setTouchable(Touchable.disabled);
            joinGameButton.setVisible(false);
            return;
        }

        if(ipTextField.getText() == null ||
                ipTextField.getText().equals("") ||
                !ipTextField.getText().matches("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$")){
            joinGameButton.setTouchable(Touchable.disabled);
            joinGameButton.setVisible(false);
            return;
        }

        if(portTextField.getText() == null ||
                portTextField.getText().equals("") ||
                !portTextField.getText().matches("^\\d*$")){
            joinGameButton.setTouchable(Touchable.disabled);
            joinGameButton.setVisible(false);
            return;
        }

        joinGameButton.setTouchable(Touchable.enabled);
        joinGameButton.setVisible(true);
    }
}
