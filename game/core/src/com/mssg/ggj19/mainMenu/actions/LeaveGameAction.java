package com.mssg.ggj19.mainMenu.actions;

import com.badlogic.gdx.Gdx;
import com.mssg.ggj19.GGJ19Main;
import com.mssg.ggj19.network.game.GameClientController;

class LeaveGameAction implements GameMenuAction {
    @Override
    public void doAction() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                GGJ19Main.THIS.goToMainMenuScreen();
            }
        });
        GameClientController.get().getClient().close();
    }
}
