package com.mssg.ggj19.mainMenu.actions;

import com.badlogic.gdx.Gdx;

class QuitGameAction implements GameMenuAction {
    @Override
    public void doAction() {
        Gdx.app.exit();
    }
}
