package com.mssg.ggj19.mainMenu.actions;

import com.mssg.ggj19.network.game.GameServerController;

class UnhostGameAction implements GameMenuAction {
    @Override
    public void doAction() {
        GameServerController.get().getServer().close();
    }
}
