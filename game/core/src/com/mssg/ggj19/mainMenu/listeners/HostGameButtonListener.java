package com.mssg.ggj19.mainMenu.listeners;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mssg.ggj19.GGJ19Main;

public class HostGameButtonListener extends ClickListener {
    @Override
    public void clicked(InputEvent event, float x, float y) {
        GGJ19Main.THIS.goToLobby();
    }
}
