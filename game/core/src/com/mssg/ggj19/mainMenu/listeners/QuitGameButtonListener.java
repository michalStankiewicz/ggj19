package com.mssg.ggj19.mainMenu.listeners;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class QuitGameButtonListener extends ClickListener {
    @Override
    public void clicked(InputEvent event, float x, float y) {
        Gdx.app.exit();
    }
}
