package com.mssg.ggj19.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.mssg.ggj19.assets.AssetsMaps;
import com.mssg.ggj19.common.input.InputMonitor;
import com.mssg.ggj19.network.game.GameClientController;

public class GameScreen implements Screen {

    private final OrthographicCamera camera;
    private GameUIStage uiStage;
    private final InputMultiplexer inputMultiplexer;

    private TiledMap map;
    private TiledMapRenderer mapRenderer;

    private Box2DDebugRenderer debugRenderer;
    private SpriteBatch batch;

    private World world;
    private GameState gameState;
    private float gameUptime = 0;

    public GameScreen() {
        uiStage = new GameUIStage();
        InputMonitor inputMonitor = new InputMonitor();
        inputMultiplexer = new InputMultiplexer();
        debugRenderer = new Box2DDebugRenderer();
        camera = new OrthographicCamera();
        camera.setToOrtho(false,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        camera.update();
        camera.translate(-10000, -10000);
        camera.zoom = 0.05f;
        batch = new SpriteBatch();
        map = AssetsMaps.MAP;
        mapRenderer = new OrthogonalTiledMapRenderer(map,1f/8);
        inputMultiplexer.addProcessor(uiStage);
        inputMultiplexer.addProcessor(inputMonitor);


    }

    @Override
    public void show() {
        gameState = GameClientController.get().getGameState();
        gameState.prepareWorld(map);
        this.world = GameClientController.get().getGameState().world;
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void render(float delta) {
        gameUptime += delta;
        Gdx.gl.glClearColor(0, 0, 0.2f, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gameState.updatePositions();
        gameState.updateItems();
        gameState.updateMyCamera(camera);

//        viewport.apply();
//        batch.setProjectionMatrix(viewport.getCamera().combined);
//        batch.begin();
//        background.draw(batch);
//        titlebg.draw(batch);
//        batch.end();
        camera.update();
        mapRenderer.setView(camera);
        mapRenderer.render();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        gameState.drawItems(delta,batch);
        gameState.drawPlayers(gameUptime, batch);

        batch.end();

//        debugRenderer.render(world, camera.combined);

        uiStage.act(delta);
        uiStage.act();
        uiStage.getViewport().apply();
        uiStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        uiStage.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
