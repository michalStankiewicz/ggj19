package com.mssg.ggj19.game.entities;

import com.mssg.ggj19.assets.AssetGraphics;

import java.util.HashMap;
import java.util.Map;

public class GameProps {
    enum ITEMS {
        ITEM_CANDLE,
        ITEM_CHAIR,
        ITEM_BOOK,
        ITEM_BURET,
        ITEM_COFFE,
        ITEM_DISH,
        ITEM_DISHES,
        ITEM_OPEN_BOOK,
        ITEM_PHYLACTERY,
        ITEM_PLATE,
        ITEM_UPSIDE_DOWN_BOOK,
        ITEM_WINE
    }

    public static Map<String, Prop> props;

    public static void preparePropProperties() {
        props = new HashMap<>();

        props.put(ITEMS.ITEM_CANDLE.name(), new Prop(2, 5,
                AssetGraphics.ITEM_CANDLE));
        props.put(ITEMS.ITEM_CHAIR.name(), new Prop(3, 15,
                AssetGraphics.ITEM_CHAIR));
        props.put(ITEMS.ITEM_BOOK.name(), new Prop(3, 16,
                AssetGraphics.ITEM_BOOK));
        props.put(ITEMS.ITEM_BURET.name(), new Prop(2, 5,
                AssetGraphics.ITEM_BURET));
        props.put(ITEMS.ITEM_COFFE.name(), new Prop(1, 9,
                AssetGraphics.ITEM_COFFE));
        props.put(ITEMS.ITEM_DISH.name(), new Prop(2, 8,
                AssetGraphics.ITEM_DISH));
        props.put(ITEMS.ITEM_DISHES.name(), new Prop(4, 7,
                AssetGraphics.ITEM_DISHES));
        props.put(ITEMS.ITEM_OPEN_BOOK.name(), new Prop(3, 17,
                AssetGraphics.ITEM_OPEN_BOOK));
        props.put(ITEMS.ITEM_PHYLACTERY.name(), new Prop(2, 3,
                AssetGraphics.ITEM_PHYLACTERY));
        props.put(ITEMS.ITEM_PLATE.name(), new Prop(2, 4,
                AssetGraphics.ITEM_PLATE));
        props.put(ITEMS.ITEM_UPSIDE_DOWN_BOOK.name(), new Prop(2, 8,
                AssetGraphics.ITEM_UPSIDE_DOWN_BOOK));
        props.put(ITEMS.ITEM_WINE.name(), new Prop(3, 15,
                AssetGraphics.ITEM_WINE));
    }
}
