package com.mssg.ggj19.game.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.mssg.ggj19.assets.AssetGraphics;
import com.mssg.ggj19.common.input.NetwordControlsCode;
import com.mssg.ggj19.game.GameState;
import com.mssg.ggj19.network.game.GameClientController;

public class PlayerEntity {

    public static final double DOUBLE_PI = 2 * Math.PI;
    private static final float ATTACK_TIMEOUT = 8;
    public boolean moving;
    private boolean attack = false;
    private float attackTimeout = -1;

    private int ownerId;
    private int team;
    private Body body;
    private Fixture fixture;
    private Animation<TextureRegion> animation;
    private Sprite spriteEyes = AssetGraphics.GHOST_EYES;
    private Sprite spriteDivinePower = AssetGraphics.EXORCIST_ATTACK;

    private static BodyDef playerBodyDef = new BodyDef(){{
        type = BodyType.DynamicBody;
        active = true;
    }};

    private static FixtureDef playerFixtureDef = new FixtureDef(){{
        CircleShape tmpShape = new CircleShape();

        tmpShape.setRadius(0.8f);
//        isSensor = true;

        shape = tmpShape;
    }};;
    private boolean dead = false;

    public static PlayerEntity add(int ownerId, int team, GameState gameState) {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.ownerId = ownerId;
        if(team == Team.EXORCISTS){
            playerFixtureDef.filter.categoryBits = CollisionType.EXORCIST;
            playerFixtureDef.filter.maskBits = CollisionType.WALL;
            playerEntity.animation = new Animation(0.15f,
                    AssetGraphics.EXORCIST_ATLAS.getRegions());
        } else if(team == Team.GHOSTS){
            playerFixtureDef.filter.categoryBits = CollisionType.GHOST;
            playerFixtureDef.filter.maskBits = CollisionType.WALL
                    | CollisionType.ITEM;
            playerEntity.animation = new Animation(0.15f,
                    AssetGraphics.GHOST_ATLAS.getRegions());
        }
        playerEntity.team = team;
        playerEntity.body = gameState.world.createBody(playerBodyDef);
        playerEntity.fixture = playerEntity.body.createFixture(playerFixtureDef);

        playerEntity.fixture.setUserData(playerEntity);

        Vector2 spawnPoint = gameState.getSpawnUtil().getSpawnFor(team);

        playerEntity.body.setTransform(spawnPoint,0);



        return playerEntity;
    }

    public void draw(float gameUptime, SpriteBatch batch) {
        if(dead){
            return;
        }
        if(team == Team.GHOSTS){
            drawGhost(gameUptime, batch);
        } else {
            drawExorcist(gameUptime, batch);
        }
    }

    private void drawExorcist(float gameUptime, SpriteBatch batch) {




        if(!moving && team == Team.EXORCISTS){
            gameUptime = 0;
        }

        batch.draw(animation.getKeyFrame(gameUptime, true),
                body.getPosition().x - 1f,
                body.getPosition().y - 1f,
                2,2);

        float ex = body.getPosition().x;
        float ey = body.getPosition().y;
        float angle = (float) (body.getAngle() - Math.PI);

        float opacity = 0;

        if(attackTimeout <= 6){
            opacity = 0;
        } else {
            opacity = (attackTimeout-6)/2;
        }

        spriteDivinePower.setSize(10,10);
        spriteDivinePower.setCenter(ex,ey);
        spriteDivinePower.setAlpha(opacity);
        spriteDivinePower.draw(batch);

    }

    private void drawGhost(float gameUptime, SpriteBatch batch) {
        float opacity = 0;

        if(attackTimeout <= 0){
            opacity = 1;
        } else {
            opacity = 0.1f + (1.0f - attackTimeout/ATTACK_TIMEOUT) * 0.6f;
        }

        Color color = batch.getColor();
        float oldAlpha = color.a;

        color.a = oldAlpha*opacity;
        batch.setColor(color);

        batch.draw(animation.getKeyFrame(gameUptime, true),
                body.getPosition().x - 1f,
                body.getPosition().y - 1f,
                2,2);


        float ex = body.getPosition().x;
        float ey = body.getPosition().y;
        float angle = (float) (body.getAngle() - Math.PI);

        ex = positionEyesForAngle(ex, angle);

        spriteEyes.setCenter(ex,ey);
        spriteEyes.draw(batch);

        color.a = oldAlpha;
        batch.setColor(color);
    }

    private float positionEyesForAngle(float ex, float angle) {
        if(angle < -3 || (angle > -0.01f && angle < 0.01f)){
            spriteEyes.setSize(2,2);
            if(angle < -5 || (angle > -0.01f && angle < 0.01f)){
                ex -= 0.2f;
            } else if(angle > -4){
                ex += 0.2f;
            }
        }
        else{
            spriteEyes.setSize(0,0);
        }
        return ex;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public void setFixture(Fixture fixture) {
        this.fixture = fixture;
    }
    public int getTeam() {
        return team;
    }

    public void setAttacking(boolean attack) {
        if(attack){
            if(!this.attack && attackTimeout <= 0){
                this.attack = attack;
                attackTimeout = ATTACK_TIMEOUT;
            }
        } else {
            this.attack = attack;
        }
    }

    public void update(float delta) {
        if(attackTimeout > 0) {
            attackTimeout -= delta;
        }

        attack = false;
    }

    public boolean isAttack(){
        return attack;
    }

    public boolean hasAttacked() {
        boolean attackCpy = attack;
        attack = false;
        return attackCpy;
    }

    public float getAttackTimeout() {
        return attackTimeout;
    }

    public void setAttackTimeout(float attackTimeout) {
        this.attackTimeout = attackTimeout;
    }

    public void markDead() {
        dead = true;
    }

    public void setDead(boolean dead){
        this.dead = dead;
    }

    public boolean isDead() {
        return dead;
    }

    public static interface Team {
        int GHOSTS = 0;
        int EXORCISTS = 1;
    }
}
