package com.mssg.ggj19.game.entities;

public interface CollisionType {
    short WALL = 1;
    short EXORCIST = 2;
    short GHOST = 4;
    short ITEM = 8;
}
