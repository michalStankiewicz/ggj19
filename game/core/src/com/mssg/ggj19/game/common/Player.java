package com.mssg.ggj19.game.common;

public class Player {
    private String name;
    private long serverId;
    private int team;

    public Player() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public long getServerId() {
        return serverId;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public int getTeam() {
        return team;
    }
}
