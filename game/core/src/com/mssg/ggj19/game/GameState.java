package com.mssg.ggj19.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mssg.ggj19.assets.AssetsProperties;
import com.mssg.ggj19.common.input.NetwordControlsCode;
import com.mssg.ggj19.game.common.Player;
import com.mssg.ggj19.game.entities.CollisionType;
import com.mssg.ggj19.game.entities.GameProps;
import com.mssg.ggj19.game.entities.PlayerEntity;
import com.mssg.ggj19.game.entities.PropEntity;
import com.mssg.ggj19.game.util.SpawnUtil;
import com.mssg.ggj19.network.game.objects.ItemsTouchedEvent;
import com.mssg.ggj19.network.game.objects.PlayerInputEvent;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.PlayerUpdateEvent;
import com.mssg.ggj19.network.game.objects.TickUpdateEvent;
import com.mssg.ggj19.network.game.server.PingPongArray;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class GameState {

    public static final float V_DIAGONAL = (float) (Math.sqrt(2) / 2);
    private static final float EXORCIST_SPEED = 12;
    private static final float GHOST_SPEED = 5;
    public static final BodyDef WALL_BODY_DEF = new BodyDef();
    public static final String WALLS_PHYSICS = "WALLS_PHYSICS";
    public static final String WALLS_BORDER = "WALLS_BORDER";
    public static final float TICK_RATE = Integer.parseInt(AssetsProperties.CONFIG
            .getProperty("multiplayer.tickMillis"))/1000f;
    public static final float EXORCIST_ATTACK_RADIUS = 14;
    public int playersReady = 0;
    private ConcurrentMap<Integer, Player> players;
    private ConcurrentMap<Integer, PlayerInputEvent> playerInputs;
    private ConcurrentMap<Integer, PlayerEntity> playerEntities;
    private ConcurrentMap<Integer, PingPongArray> pingPong;
    private ConcurrentMap<Integer, PlayerUpdateEvent> playerUpdates;
    private String me;
    private int myId;
    private boolean gameOn;
    private AtomicBoolean listeningEnabled;

    private SpawnUtil spawnUtil;

    public World world;

    public static final FixtureDef WALL_FIXTURE_DEF = new FixtureDef();
    private int myTeam;
    private ConcurrentMap<Integer, PropEntity> itemsEntities;
    private ConcurrentMap<Integer, Float> itemsUpdates;

    public GameState() {
        players = new ConcurrentHashMap<>();
        playerInputs = new ConcurrentHashMap<>();
        playerEntities = new ConcurrentHashMap<>();
        pingPong = new ConcurrentHashMap<>();
        listeningEnabled = new AtomicBoolean(false);
        playerUpdates = new ConcurrentHashMap<>();
        spawnUtil = new SpawnUtil();
        itemsEntities = new ConcurrentHashMap<>();
        itemsUpdates = new ConcurrentHashMap<>();
        reset();
    }

    public void reset() {
        players.clear();
        pingPong.clear();
        spawnUtil.reset();
        playersReady = 0;
    }

    public void prepareWorld(TiledMap map) {
        this.world = new World(Vector2.Zero, true);

        addWalls(map, WALLS_PHYSICS);
        addWalls(map, WALLS_BORDER);
        addItems(map);

        MapLayer spawnPoints = map.getLayers().get("SPAWN_POINTS");

        for(MapObject spawnPoint : spawnPoints.getObjects()){
//            BodyDef wallDef = new BodyDef();
            float x = (float) spawnPoint.getProperties().get("x")/16;
            float y = (float) spawnPoint.getProperties().get("y")/16;
            float width = (float) spawnPoint.getProperties().get("width")/16;
            float height = (float) spawnPoint.getProperties().get("height")/16;


            float spawnX = 2 * x + width;
            float spawnY = 2 * y + height;
            spawnUtil.addSpawn((String) spawnPoint.getProperties().get("type"),
                    new Vector2(spawnX,spawnY));
        }
    }

    private void addItems(TiledMap map) {
        MapLayer wallsLayer = map.getLayers().get("ITEM_COL");

        for(MapObject item : wallsLayer.getObjects()){
            String type = (String) item.getProperties().get("type");

            if(!GameProps.props.containsKey(type)){
                continue;
            }
            int id = (int) item.getProperties().get("id");
            itemsEntities.put(id, PropEntity.create(id, item, world));


        }
    }

    private void addWalls(TiledMap map, String layer) {
        MapLayer wallsLayer = map.getLayers().get(layer);

        for(MapObject wall : wallsLayer.getObjects()){
            float x = (float) wall.getProperties().get("x")/16;
            float y = (float) wall.getProperties().get("y")/16;
            float width = (float) wall.getProperties().get("width")/16;
            float height = (float) wall.getProperties().get("height")/16;


            WALL_BODY_DEF.position.x = 2*x + width;
            WALL_BODY_DEF.position.y = 2*y + height;
            WALL_BODY_DEF.type = BodyDef.BodyType.StaticBody;


            PolygonShape shape = new PolygonShape();
            shape.setAsBox(width,height);
            WALL_FIXTURE_DEF.shape = shape;

            if(layer.equals(WALLS_BORDER)) {
                WALL_FIXTURE_DEF.filter.categoryBits = CollisionType.WALL;
                WALL_FIXTURE_DEF.filter.maskBits = CollisionType.EXORCIST
                        | CollisionType.GHOST;
            } else if(layer.equals(WALLS_PHYSICS)){
                WALL_FIXTURE_DEF.filter.categoryBits = CollisionType.WALL;
                WALL_FIXTURE_DEF.filter.maskBits = CollisionType.EXORCIST;
            }



            world.createBody(WALL_BODY_DEF)
                    .createFixture(WALL_FIXTURE_DEF)
                    .getFilterData();
        }
    }


    public void addPlayer(int id, Player player) {
        players.put(id, player);
        pingPong.put(id, new PingPongArray());
        if(me != null && me.equals(player.getName())){
            setMyId(id);
        }

    }
    public void addPlayerPhysics(int id, int team) {
        if(id == getMyId()){
            setMyTeam(team);
        }
        players.get(id).setTeam(team);
        playerEntities.put(id, PlayerEntity.add(id, team,this));
    }

    public ConcurrentMap<Integer, Player> getPlayers() {
        return players;
    }

    public void updatePlayers(Map<Integer, Player> players) {
        for(int id: this.players.keySet()){
            if(!players.containsKey(id)){
                this.players.remove(id);
                this.pingPong.remove(id);
            }
        }

        for (int id: players.keySet()) {
            if(!this.players.containsKey(id)){
                this.players.put(id, players.get(id));
                this.pingPong.put(id, new PingPongArray());
            }
        }

    }

    public String getMe() {
        return me;
    }

    public int getMyId(){
        return myId;
    }

    public void setMe(String me) {
        this.me = me;
    }

    public void setMyId(int id){
        this.myId = id;
    }

    public boolean isGameOn() {
        return gameOn;
    }

    public void setGameOn(boolean gameOn) {
        this.gameOn = gameOn;
    }

    public ConcurrentMap<Integer, PingPongArray> getPingPong() {
        return pingPong;
    }

    public void syncTick() {
        for(int id: pingPong.keySet()){
            TickUpdateEvent currentTickEvent = new TickUpdateEvent();
            long tick = GameServerController.get().getTickCounter().getTick();
            currentTickEvent.setTick(pingPong.get(id).calculateDelay() + tick);
            GameServerController.get().getServer()
                    .sendToTCP(id, currentTickEvent);
        }
    }

    public void setListeningEnabled(boolean val) {
        listeningEnabled.set(val);
    }

    public boolean getListeningEnabled() {
        return listeningEnabled.get();
    }

    public void bufferPlayerUpdate(PlayerUpdateEvent playerUpdateEvent) {
        int id = playerUpdateEvent.getId();
        if(null == playerUpdates.get(id) ||
                playerUpdates.get(id).getTick() < playerUpdateEvent.getTick()) {
            playerUpdates.put(id, playerUpdateEvent);
        }
    }

    public SpawnUtil getSpawnUtil() {
        return spawnUtil;
    }

    public void bufferInput(int id, PlayerInputEvent playerInputEvent) {
        if(null == playerInputs.get(id) ||
                playerInputs.get(id).getTick() < playerInputEvent.getTick()) {
            playerInputs.put(id, playerInputEvent);
        }
    }

    public void simulateTicks() {
        if(gameOn) {
            long currentTick = GameServerController.get().getTickCounter().getTick();
            simulateTicks(currentTick);
        }
    }

    private void simulateTicks(long lastTick) {
        for (int key : playerInputs.keySet()) {
            if (!playerEntities.containsKey(key)) {
                break;
            }

            PlayerEntity playerEntity = playerEntities.get(key);
            playerEntity.update(TICK_RATE);
            Body body = playerEntity.getBody();
            if(playerEntity.isDead()){
                continue;
            }
            byte controls = playerInputs.get(key).getControls();
            boolean up = (controls & NetwordControlsCode.UP) > 0;
            boolean down = (controls & NetwordControlsCode.DOWN) > 0;
            boolean left = (controls & NetwordControlsCode.LEFT) > 0;
            boolean right = (controls & NetwordControlsCode.RIGHT) > 0;

            float vx = 0;
            float vy = 0;

            float speed;

            if (players.get(key).getTeam() == PlayerEntity.Team.EXORCISTS) {
                speed = EXORCIST_SPEED;
            } else {
                speed = GHOST_SPEED;
            }




            if((controls & NetwordControlsCode.ACTION_1) > 0){
                playerEntity.setAttacking(true);
                Vector2 exorcistPos = playerEntity.getBody().getPosition();
                for(int ghostFindKey: playerEntities.keySet()){
                    if(ghostFindKey == key){
                        continue;
                    }

                    PlayerEntity entity = playerEntities.get(ghostFindKey);
                    if(entity.getTeam() == PlayerEntity.Team.GHOSTS){
                        Vector2 ghostPos = entity.getBody().getPosition();

                        float length = Vector2.len2(ghostPos.x - exorcistPos.x,
                                ghostPos.y - exorcistPos.y);
                        System.out.println(length);
                        if(length < EXORCIST_ATTACK_RADIUS){
                            entity.markDead();
                        }
                    }
                }


            } else {
                playerEntity.setAttacking(false);
            }

            if (up) {
                vy += speed;
            }

            if (down) {
                vy -= speed;
            }

            if (right) {
                vx += speed;
            }

            if (left) {
                vx -= speed;
            }

            if ((vx * vy) != 0) {
                vx *= V_DIAGONAL;
                vy *= V_DIAGONAL;
            }

            body.setLinearVelocity(vx, vy);
            body.setTransform(body.getPosition(),
                    (float) Math.atan2(vy, vx));
        }
        world.step(TICK_RATE, 6, 2);

        for (int id : playerEntities.keySet()) {
            PlayerUpdateEvent playerUpdateEvent = new PlayerUpdateEvent();

            playerUpdateEvent.setTick(lastTick + 1);
            playerUpdateEvent.setId(id);
            playerUpdateEvent.setControls(playerInputs.get(id).getControls());
            Vector2 position = playerEntities.get(id).getBody().getPosition();
            playerUpdateEvent.setX(position.x);
            playerUpdateEvent.setY(position.y);

            playerUpdateEvent.setAttackTimeout(playerEntities.get(id).getAttackTimeout());
            playerUpdateEvent.setDead(playerEntities.get(id).isDead());


            playerUpdateEvent.setRotationDeg(playerEntities.get(id).getBody().getAngle());
//            System.out.println(playerUpdateEvent.getRotationDeg());
            GameServerController.get().getServer()
                    .sendToAllUDP(playerUpdateEvent);
        }

        ItemsTouchedEvent itemsTouchedEvent = new ItemsTouchedEvent();
        for (int id : itemsEntities.keySet()) {
            PropEntity propEntity = itemsEntities.get(id);

            if (propEntity.getTouchedByGhost()) {
                PlayerEntity ghost = playerEntities.get(propEntity.getGhostId());

                if(ghost.hasAttacked()){
                    System.out.println("hit");
                    if(propEntity.hit()){
                        ghost.setAttacking(false);
                        System.out.println("HIT");
                        world.destroyBody(propEntity.getBody());
                        propEntity.markDead();
                        PropKillEvent itemKilled = new PropKillEvent();
                        itemKilled.itemId = propEntity.getId();
                        GameServerController.get().getServer().sendToAllUDP(itemKilled);
                    }
                }

                propEntity.decreaseTouched(TICK_RATE);
                itemsTouchedEvent.itemId = id;
                itemsTouchedEvent.timeoutLeft = propEntity.getShakeTimeoutLeft();
                itemsTouchedEvent.ghostId = propEntity.getGhostId();
                GameServerController.get().getServer()
                        .sendToAllUDP(itemsTouchedEvent);
            }
        }

    }

    public void updateMyCamera(OrthographicCamera camera) {
        if(playerUpdates.containsKey(getMyId())) {
            camera.position.x = playerUpdates.get(getMyId()).getX();
            camera.position.y = playerUpdates.get(getMyId()).getY();
        }
    }

    private long findFirstTickInBuffer(long currentTick) {
        long tickTmp = currentTick;
        for(int id: playerInputs.keySet()){
            if(playerInputs.get(id).getTick() < tickTmp){
                tickTmp = playerInputs.get(id).getTick();
            }
        }
        return tickTmp;
    }

    public void addPlayerPhysics(ConcurrentMap<Integer, Integer> playerTeams) {
        for(int key: playerTeams.keySet()){
            addPlayerPhysics(key, playerTeams.get(key));
        }
    }

    public void updatePositions() {
        for(int id: playerUpdates.keySet()){
            PlayerUpdateEvent playerUpdateEvent = playerUpdates.get(id);

            float x = playerUpdateEvent.getX();
            float y = playerUpdateEvent.getY();
            PlayerEntity player = playerEntities.get(id);
            float r =0;
            if(player.getBody() != null) {
                r = player.getBody().getAngle();
            }
            if(playerUpdateEvent.getControls() > 0){
                r = playerUpdateEvent.getRotationDeg();
                player.moving = true;
            } else{
                player.moving = false;
            }
            player.setAttackTimeout(playerUpdateEvent.getAttackTimeout());
            player.getBody().setTransform(x,y,r);
            player.setDead(playerUpdateEvent.getDead());
//            if(playerUpdates.get(id).getTick() < tickTmp){
//                tickTmp = playerUpdates.get(id).getTick();

//            }
        }
    }

    public void drawPlayers(float gameUptime, SpriteBatch batch) {
        for(int id: playerEntities.keySet()){
            if(myTeam == PlayerEntity.Team.EXORCISTS
                    && players.get(id).getTeam() == PlayerEntity.Team.GHOSTS){
                continue; //exorcists cant see ghosts
            }
            playerEntities.get(id).draw(gameUptime, batch);
        }
    }

    public void setMyTeam(int myTeam) {
        this.myTeam = myTeam;
    }

    public int getMyTeam() {
        return myTeam;
    }

    public void prepareListener() {
        world.setContactListener(new GenericContactListener());
    }

    public void drawItems(float delta, SpriteBatch batch) {
        for(int key: itemsEntities.keySet()){
            itemsEntities.get(key).draw(delta, batch);
        }
    }

    public void bufferItemUpdates(ItemsTouchedEvent itemTouchEvent) {
        itemsEntities.get(itemTouchEvent.itemId)
                .setShakeTimeOut(itemTouchEvent.timeoutLeft);
        itemsEntities.get(itemTouchEvent.itemId)
                .seGhostId(itemTouchEvent.ghostId);
    }

    public void updateItems() {
        while(!propsToKill.isEmpty()){
            int itemId = propsToKill.poll();
            PropEntity item = itemsEntities.get(itemId);
            world.destroyBody(item.getBody());
            itemsEntities.remove(itemId);
        }
    }
    ConcurrentLinkedQueue<Integer> propsToKill = new ConcurrentLinkedQueue<>();
    public void killProp(int itemId) {
        propsToKill.add(itemId);
    }
}
