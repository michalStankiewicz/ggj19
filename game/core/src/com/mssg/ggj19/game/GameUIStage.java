package com.mssg.ggj19.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mssg.ggj19.common.ui.ChatView;

public class GameUIStage extends Stage {

    public GameUIStage() {
        addActor(ChatView.get());
    }

    @Override
    public boolean keyDown(int keyCode) {
        if(Input.Keys.GRAVE == keyCode){
            if(getActors().contains(ChatView.get(), true)){
                ChatView.get().remove();
            } else {
                addActor(ChatView.get());
            }
        }
        return false;
    }
}
