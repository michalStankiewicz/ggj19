package com.mssg.ggj19.game;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.mssg.ggj19.game.entities.PlayerEntity;
import com.mssg.ggj19.game.entities.PropEntity;

class GenericContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Object data1 = contact.getFixtureA().getUserData();
        Object data2 = contact.getFixtureB().getUserData();

        checkTouch(data1, data2);
        checkTouch(data2, data1);

        // if ghost && item
        // -> set enable destroy
        // disable destroy on others items
    }

    private void checkTouch(Object data1, Object data2) {
        if(data1 instanceof PlayerEntity
                && data2 instanceof PropEntity){
            PlayerEntity playerEntity = (PlayerEntity) data1;
            if(playerEntity.isDead()){
                return;
            }
            if(playerEntity.getTeam() == PlayerEntity.Team.GHOSTS) {
                PropEntity propEntity = (PropEntity) data2;
                propEntity.setTouchedByGhost(true, playerEntity.getOwnerId());
            }

        }
    }

    @Override
    public void endContact(Contact contact) {
        Object data1 = contact.getFixtureA().getUserData();
        Object data2 = contact.getFixtureB().getUserData();

        checkUntouch(data1, data2);
        checkUntouch(data2, data1);
    }


    private void checkUntouch(Object data1, Object data2) {
        if(data1 instanceof PlayerEntity
                && data2 instanceof PropEntity){
            PlayerEntity playerEntity = (PlayerEntity) data1;
            if(playerEntity.isDead()){
                return;
            }
            if(playerEntity.getTeam() == PlayerEntity.Team.GHOSTS) {
                PropEntity propEntity = (PropEntity) data2;
                propEntity.setTouchedByGhost(false, playerEntity.getOwnerId());
            }

        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
