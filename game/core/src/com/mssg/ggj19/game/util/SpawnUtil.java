package com.mssg.ggj19.game.util;

import com.badlogic.gdx.math.Vector2;
import com.mssg.ggj19.game.entities.PlayerEntity;

import java.util.ArrayList;
import java.util.List;

public class SpawnUtil {

    public List<Vector2> exorcistSpawns;
    public List<Vector2> ghostSpawns;

    public SpawnUtil() {
        ghostSpawns = new ArrayList<>();
        exorcistSpawns = new ArrayList<>();
    }

    public Vector2 getSpawnFor(int team) {
        switch(team){
            case PlayerEntity.Team.EXORCISTS:
                return exorcistSpawns.get((int)(Math.random() * exorcistSpawns.size()));
            case PlayerEntity.Team.GHOSTS:
                return ghostSpawns.get((int)(Math.random() * ghostSpawns.size()));
        }
        return Vector2.Zero;
    }

    public void reset() {
        ghostSpawns.clear();
        exorcistSpawns.clear();
    }

    public void addSpawn(String type, Vector2 position) {
        if("GHOST".equals(type)){
            ghostSpawns.add(position);
        }
        if("EXORCIST".equals(type)){
            exorcistSpawns.add(position);
        }
    }
}
