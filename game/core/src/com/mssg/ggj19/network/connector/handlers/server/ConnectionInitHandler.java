package com.mssg.ggj19.network.connector.handlers.server;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.network.connector.ConnectorServerController;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.connector.objects.Ping;
import com.mssg.ggj19.network.connector.objects.PlayerInvitationEvent;
import com.mssg.ggj19.network.connector.objects.InvalidPasswordEvent;
import com.mssg.ggj19.network.connector.objects.ConnectionInitEvent;
import com.mssg.ggj19.network.game.GameServerController;

import java.util.Timer;
import java.util.TimerTask;

public class ConnectionInitHandler implements MessageHandler<ConnectionInitEvent> {

    @Override
    public void processMessage(Connection connection, ConnectionInitEvent connectionInitEvent) {
        if(ConnectorServerController.get().passwordInvalid(connectionInitEvent.getPassword())){
            connection.sendTCP(new InvalidPasswordEvent());
            return;
        };

        /*
        TODO
        2. Check username
            If taken - generate new (with _1 or something)

        3. Send PlayerInvitationEvent:
            tcpPort, udpPort, new playername
         */
        PlayerInvitationEvent playerInvitationEvent = new PlayerInvitationEvent();

        playerInvitationEvent.setPlayerName(connectionInitEvent.getName());
        playerInvitationEvent.setHost(connectionInitEvent.getTargetHost());
        playerInvitationEvent.setTcpPort(GameServerController.get().getTcpPort());
        playerInvitationEvent.setUdpPort(GameServerController.get().getUdpPort());

        connection.sendTCP(playerInvitationEvent);


    }
}
