package com.mssg.ggj19.network.connector;

import com.esotericsoftware.kryonet.KryoSerialization;
import com.esotericsoftware.kryonet.Server;
import com.mssg.ggj19.common.ui.ChatView;
import com.mssg.ggj19.network.common.EventListener;
import com.mssg.ggj19.network.NetworkUtil;

import java.io.IOException;

public class ConnectorServerController {

    private static final ConnectorServerController THIS = new ConnectorServerController();

    public static final ConnectorServerController get(){
        return THIS;
    }

    private Server connectorServer;
    private String serverPassword;
    private int tcpPort;

    public ConnectorServerController() {
        connectorServer = new Server(1024, 1024, new KryoSerialization());
        connectorServer.addListener(EventListener.get());
        connectorServer.start();
    }

    public void startConnectorServer(String password) {
        try {
            tcpPort = NetworkUtil.findFreePort();
            serverPassword = password;
            connectorServer.bind(tcpPort);
            System.out.println("Connector server up at port " + tcpPort);
            ChatView.get().addChatMessage("Server up at port: " + tcpPort);
        } catch (IOException e) {
            //TODO handle server start failed
            e.printStackTrace();
        }
    }

    public Server getConnectorServer() {
        return connectorServer;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public boolean passwordInvalid(String password) {
        if(null == serverPassword || "".equals(serverPassword)){
            return false;
        }

        if(serverPassword.equals(password)){
            return false;
        }

        return true;
    }
}
