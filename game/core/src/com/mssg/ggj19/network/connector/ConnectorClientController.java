package com.mssg.ggj19.network.connector;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.KryoSerialization;
import com.mssg.ggj19.assets.AssetsProperties;
import com.mssg.ggj19.mainMenu.menu.MenuStage;
import com.mssg.ggj19.network.common.EventListener;
import com.mssg.ggj19.network.connector.objects.ConnectionInitEvent;
import com.mssg.ggj19.network.game.GameClientController;

import java.io.IOException;

public class ConnectorClientController {

    private static final ConnectorClientController THIS = new ConnectorClientController();
    public static final int TIMEOUT = Integer.parseInt(AssetsProperties.CONFIG.getProperty("multiplayer.timeout"));

    public static final ConnectorClientController get(){
        return THIS;
    }

    private Client connectorClient;

    public ConnectorClientController() {
        connectorClient = new Client(1024, 1024, new KryoSerialization());
        connectorClient.addListener(EventListener.get());
        connectorClient.start();
    }

    public void joinLocal(ConnectionInitEvent connectionInitEvent) {
        join("127.0.0.1", ConnectorServerController.get().getTcpPort(), connectionInitEvent);
    }

    public void join(String host, int tcpPort, ConnectionInitEvent connectionInitEvent) {
        GameClientController.get().getGameState().reset();
        try {
            connectorClient.connect(TIMEOUT, host, tcpPort);
        } catch (IOException ex) {
            MenuStage.getInstance().showError("Can't connect");
            ex.printStackTrace();
        }

        connectorClient.sendTCP(connectionInitEvent);
    }

    public Client getConnectorClient() {
        return connectorClient;
    }
}
