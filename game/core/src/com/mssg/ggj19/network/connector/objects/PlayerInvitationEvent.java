package com.mssg.ggj19.network.connector.objects;

import com.mssg.ggj19.network.common.Event;

public class PlayerInvitationEvent extends Event {

    private String host;
    private int tcpPort;
    private int udpPort;
    private String playerName;

    public PlayerInvitationEvent() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public void setTcpPort(int tcpPort) {
        this.tcpPort = tcpPort;
    }

    public int getUdpPort() {
        return udpPort;
    }

    public void setUdpPort(int udpPort) {
        this.udpPort = udpPort;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
