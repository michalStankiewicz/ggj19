package com.mssg.ggj19.network.connector.objects;

import com.badlogic.gdx.utils.Pool;
import com.mssg.ggj19.network.common.Event;

public class ConnectionInitEvent extends Event {

    String name;
    String password;
    private String targetHost;

    public ConnectionInitEvent() {
    }

    public ConnectionInitEvent(String name, String password, String targetHost) {
        this.name = name;
        this.password = password;
        this.targetHost = targetHost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTargetHost() {
        return targetHost;
    }

    public void setTargetHost(String targetHost) {
        this.targetHost = targetHost;
    }
}
