package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

public class PlayerUpdateEvent extends Event {
    private float x;
    private float y;
    private float rotationDeg;
    private float attackTimeout;

    private byte controls;
    private long tick;
    private int id;
    private boolean dead;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public byte getControls() {
        return controls;
    }

    public void setControls(byte controls) {
        this.controls = controls;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }

    public long getTick() {
        return tick;
    }

    public float getRotationDeg() {
        return rotationDeg;
    }

    public void setRotationDeg(float rotationDeg) {
        this.rotationDeg = rotationDeg;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public float getAttackTimeout() {
        return attackTimeout;
    }

    public void setAttackTimeout(float attackTimeout) {
        this.attackTimeout = attackTimeout;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public boolean getDead() {
        return dead;
    }
}
