package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

import java.util.HashMap;
import java.util.concurrent.ConcurrentMap;

public class StartGameEvent extends Event {
    private ConcurrentMap<Integer,Integer> teams;

    public ConcurrentMap<Integer, Integer> getTeams() {
        return teams;
    }

    public void setTeams(ConcurrentMap<Integer, Integer> teams) {
        this.teams = teams;
    }
}
