package com.mssg.ggj19.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.game.common.Player;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.connector.objects.Ping;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.PlayerLoadedEvent;
import com.mssg.ggj19.network.game.server.PingPongArray;

import java.util.Timer;
import java.util.TimerTask;

public class PlayerLoadedHandler implements MessageHandler<PlayerLoadedEvent> {
    @Override
    public void processMessage(Connection connection, PlayerLoadedEvent playerLoadedEvent) {
        GameServerController.get().runTickSynchro(connection);
    }
}
