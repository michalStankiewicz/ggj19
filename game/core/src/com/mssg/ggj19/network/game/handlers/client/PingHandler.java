package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.connector.objects.Ping;
import com.mssg.ggj19.network.game.objects.Pong;

public class PingHandler implements MessageHandler<Ping> {
    @Override
    public void processMessage(Connection connection, Ping ping) {
        connection.sendUDP(new Pong(ping.getId(), ping.getTick()));
    }
}
