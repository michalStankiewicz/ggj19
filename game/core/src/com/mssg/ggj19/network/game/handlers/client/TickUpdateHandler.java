package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.common.ui.ChatView;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.objects.TickReceivedEvent;
import com.mssg.ggj19.network.game.objects.TickUpdateEvent;

public class TickUpdateHandler implements MessageHandler<TickUpdateEvent> {
    @Override
    public void processMessage(Connection connection, TickUpdateEvent tickUpdateEvent) {
        GameClientController.get().getTickCounter().setTick(tickUpdateEvent.getTick());
        ChatView.get().addChatMessage("Tick updated: " + tickUpdateEvent.getTick());
        TickReceivedEvent tickReceivedEvent = new TickReceivedEvent();
        GameClientController.get().getClient().sendTCP(tickReceivedEvent);
    }
}
