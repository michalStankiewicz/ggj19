package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

public class PlayerSendMessageEvent extends Event {
    private String message;
    private String player;

    public PlayerSendMessageEvent() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getPlayer() {
        return player;
    }
}
