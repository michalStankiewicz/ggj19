package com.mssg.ggj19.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.GGJ19Main;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.objects.GameReadyEvent;

public class GameReadyHandler implements MessageHandler<GameReadyEvent> {
    @Override
    public void processMessage(Connection connection, GameReadyEvent gameReadyEvent) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                GGJ19Main.THIS.goToGameScreen();
            }
        });
    }
}
