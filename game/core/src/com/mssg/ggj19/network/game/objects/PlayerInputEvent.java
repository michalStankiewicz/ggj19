package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;


public class PlayerInputEvent extends Event {

    private byte controls;
    private long tick;

    public byte getControls() {
        return controls;
    }

    public void setControls(byte controls) {
        this.controls = controls;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }

    public long getTick() {
        return tick;
    }

    @Override
    public String toString() {
        return "PlayerInputEvent{" +
                "controls=" + controls +
                ", tick=" + tick +
                '}';
    }
}
