package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ItemsTouchedEvent extends Event{

    public int itemId;
    public float timeoutLeft;
    public int ghostId;
}
