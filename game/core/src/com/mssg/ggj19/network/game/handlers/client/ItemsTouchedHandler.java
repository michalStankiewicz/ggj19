package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.objects.ItemsTouchedEvent;

public class ItemsTouchedHandler implements MessageHandler<ItemsTouchedEvent> {
    @Override
    public void processMessage(Connection connection, ItemsTouchedEvent itemsTouchedEvent) {
        GameClientController.get().getGameState().bufferItemUpdates(itemsTouchedEvent);
    }
}
