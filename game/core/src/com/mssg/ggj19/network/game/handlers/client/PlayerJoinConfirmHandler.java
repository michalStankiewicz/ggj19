package com.mssg.ggj19.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.GGJ19Main;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.objects.PlayerJoinConfirmEvent;

public class PlayerJoinConfirmHandler implements MessageHandler<PlayerJoinConfirmEvent> {
    @Override
    public void processMessage(Connection connection, PlayerJoinConfirmEvent playerJoinConfirmEvent) {
        GameClientController.get().getGameState().setMyId(playerJoinConfirmEvent.getConnectionId());
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                GGJ19Main.THIS.goToLobby();
            }
        });
    }
}
