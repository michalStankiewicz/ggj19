package com.mssg.ggj19.network.game;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.mssg.ggj19.network.game.objects.ChatEvent;
import com.mssg.ggj19.network.game.objects.PlayersListEvent;

class PlayerDisconnectListener extends Listener {
    private static final PlayerDisconnectListener THIS = new PlayerDisconnectListener();

    public static Listener get() {
        return THIS;
    }

    @Override
    public void disconnected(Connection connection) {
        super.disconnected(connection);
        ChatEvent chatEvent = new ChatEvent();

        String playerName = GameServerController.get().getGameState()
                .getPlayers().get(connection.getID()).getName();
        chatEvent.setMessage("Player " + playerName + " disconnected");
        GameServerController.get().getServer()
                .sendToAllTCP(chatEvent);

        GameServerController.get().getGameState()
                .getPlayers().remove(connection.getID());

        PlayersListEvent playersList = new PlayersListEvent();
        playersList.setPlayers(GameServerController.get().getGameState().getPlayers());
        GameServerController.get().getServer()
                .sendToAllTCP(playersList);
    }
}
