package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

public class PlayerJoinConfirmEvent extends Event {
    private int connectionId;

    public PlayerJoinConfirmEvent() {
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public int getConnectionId() {
        return connectionId;
    }
}
