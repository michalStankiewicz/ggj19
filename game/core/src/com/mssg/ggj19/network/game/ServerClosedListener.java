package com.mssg.ggj19.network.game;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.mssg.ggj19.GGJ19Main;
import com.mssg.ggj19.mainMenu.menu.MenuStage;

class ServerClosedListener extends Listener {
    private static final ServerClosedListener THIS = new ServerClosedListener();

    public static Listener get() {
        return THIS;
    }

    @Override
    public void disconnected(Connection connection) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                GGJ19Main.THIS.goToMainMenuScreen();
                if(!GameServerController.get().amIHost()) {
                    MenuStage.getInstance().showError("Server down :(");
                }
            }
        });
    }
}
