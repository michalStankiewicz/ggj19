package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

public class PlayerJoinEvent extends Event {
    private String playerName;

    public PlayerJoinEvent() {
    }

    public PlayerJoinEvent(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
