package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.assets.AssetsMaps;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.objects.StartGameEvent;

public class StartGameHandler implements MessageHandler<StartGameEvent> {
    @Override
    public void processMessage(Connection connection, StartGameEvent startGameEvent) {
        GameClientController.get().getGameState().setListeningEnabled(true);
        GameClientController.get().getGameState().addPlayerPhysics(startGameEvent.getTeams());
//        GameClientController.get().getGameState().addTheRestOfPlayerPhysics();
    }
}
