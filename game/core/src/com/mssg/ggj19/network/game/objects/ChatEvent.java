package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

public class ChatEvent extends Event {

    private String message;

    public ChatEvent() {
    }

    public ChatEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
