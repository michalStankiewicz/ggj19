package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.common.ui.ChatView;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.objects.ChatEvent;

public class ChatHandler implements MessageHandler<ChatEvent> {
    @Override
    public void processMessage(Connection connection, ChatEvent chatEvent) {
        //TODO clean up events/messages
        ChatView.get().addChatMessage(chatEvent.getMessage());
    }
}
