package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

public class Pong extends Event {
    private int id;
    private long tick;

    public Pong() {

    }

    public Pong(int id, long tick) {
        this.id = id;
        this.tick = tick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTick() {
        return tick;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }
}
