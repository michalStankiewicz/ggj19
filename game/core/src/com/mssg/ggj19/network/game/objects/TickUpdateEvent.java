package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.network.common.Event;

public class TickUpdateEvent extends Event {
    private long tick;

    public void setTick(long tick) {
        this.tick = tick;
    }

    public long getTick() {
        return tick;
    }
}
