package com.mssg.ggj19.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.GGJ19Main;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.objects.PlayerJoinEvent;
import com.mssg.ggj19.network.game.objects.PlayersListEvent;

public class PlayersListHandler implements MessageHandler<PlayersListEvent> {

    @Override
    public void processMessage(Connection connection, PlayersListEvent playersListEvent) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                GameClientController.get().getGameState()
                        .updatePlayers(playersListEvent.getPlayers());
            }
        });
    }
}
