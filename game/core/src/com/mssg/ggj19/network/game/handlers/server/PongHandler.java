package com.mssg.ggj19.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.Pong;
import com.mssg.ggj19.network.game.server.PingPongArray;

public class PongHandler implements MessageHandler<Pong> {
    @Override
    public void processMessage(Connection connection, Pong pong) {
        pong.setTick(GameServerController.get().getTickCounter().getTick());
        PingPongArray pingPong = GameServerController.get().getGameState()
                .getPingPong().get(connection.getID());

        pingPong.addPong(pong);
    }
}
