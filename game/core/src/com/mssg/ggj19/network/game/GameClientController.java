package com.mssg.ggj19.network.game;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.KryoSerialization;
import com.mssg.ggj19.assets.AssetsProperties;
import com.mssg.ggj19.game.GameState;
import com.mssg.ggj19.network.common.EventListener;
import com.mssg.ggj19.network.connector.objects.PlayerInvitationEvent;
import com.mssg.ggj19.network.game.objects.PlayerJoinEvent;

import java.io.IOException;

public class GameClientController {
    private static final GameClientController THIS = new GameClientController();
    public static final int TIMEOUT = Integer.parseInt(AssetsProperties.CONFIG.getProperty("multiplayer.timeout"));
    private GameState gameState;
    private TickCounter tickCounter = new ClientTickCounter();

    public static GameClientController get() {
        return THIS;
    }

    private Client client;

    public GameClientController() {
        client = new Client(1024, 1024, new KryoSerialization());
        client.addListener(EventListener.get());
        client.addListener(ServerClosedListener.get());
        gameState = new GameState();
        client.start();
    }

    public void join(PlayerInvitationEvent playerInvitationEvent) {


        try {
            client.connect(TIMEOUT, playerInvitationEvent.getHost(),
                    playerInvitationEvent.getTcpPort(),
                    playerInvitationEvent.getUdpPort());
            tickCounter.start();
            PlayerJoinEvent playerJoinEvent = new PlayerJoinEvent();
            playerJoinEvent.setPlayerName(playerInvitationEvent.getPlayerName());
            client.sendTCP(playerJoinEvent);
            getGameState().setMe(playerInvitationEvent.getPlayerName());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Client getClient() {
        return client;
    }

    public GameState getGameState() {
        return gameState;
    }

    public TickCounter getTickCounter() {
        return tickCounter;
    }
}
