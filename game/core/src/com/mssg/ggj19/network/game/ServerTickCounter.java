package com.mssg.ggj19.network.game;

public class ServerTickCounter extends TickCounter {

    @Override
    protected void doExtraStuff() {
        GameServerController.get().getGameState().simulateTicks();
    }
}
