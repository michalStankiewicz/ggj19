package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.game.PropKillEvent;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameClientController;

public class PropKillHandler implements MessageHandler<PropKillEvent> {
    @Override
    public void processMessage(Connection connection, PropKillEvent propKillEvent) {
        GameClientController.get().getGameState().killProp(propKillEvent.itemId);
    }
}
