package com.mssg.ggj19.network.game;

import com.mssg.ggj19.assets.AssetsProperties;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class TickCounter {
    public static final int TICK_MILLIS = Integer.parseInt(AssetsProperties.CONFIG.getProperty("multiplayer.tickMillis"));
    private AtomicLong tick;

    private static final ScheduledExecutorService tickCounterScheduler = Executors.newScheduledThreadPool(1);

    private ScheduledFuture<?> tickCounter = null;

    public TickCounter() {
        tick = new AtomicLong(0L);
    }

    public void start(){
        if(tickCounter != null) {
            tickCounter.cancel(false);
        }

        tickCounter = tickCounterScheduler.scheduleAtFixedRate(
                () -> run(),
                0,
                TICK_MILLIS,
                TimeUnit.MILLISECONDS
        );
    }

    public void run() {
        tick.incrementAndGet();
        doExtraStuff();
    }

    protected void doExtraStuff() {

    }

    public long getTick() {
        return tick.get();
    }

    public void setTick(long tick) {
        this.tick.set(tick);
    }
}
