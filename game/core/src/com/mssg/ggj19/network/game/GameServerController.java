package com.mssg.ggj19.network.game;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.KryoSerialization;
import com.esotericsoftware.kryonet.Server;
import com.mssg.ggj19.assets.AssetsMaps;
import com.mssg.ggj19.game.GameState;
import com.mssg.ggj19.network.common.EventListener;
import com.mssg.ggj19.network.NetworkUtil;
import com.mssg.ggj19.network.connector.objects.Ping;
import com.mssg.ggj19.network.game.server.PingPongArray;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class GameServerController {

    private static final GameServerController THIS = new GameServerController();
    private GameState gameState = new GameState();
    private AtomicBoolean iAmHost = new AtomicBoolean(false);
    private TickCounter tickCounter = new ServerTickCounter();

    public static final GameServerController get() {
        return THIS;
    }

    private Server server;
    private int tcpPort;

    private int udpPort;

    public GameServerController() {
        server = new Server(1024,1024, new KryoSerialization());
        server.addListener(EventListener.get());
        server.addListener(PlayerDisconnectListener.get());
        server.start();
    }

    public void startGameServer(String serverPassword) {
        gameState.reset();
        try {
            tcpPort = NetworkUtil.findFreePort();
            udpPort = NetworkUtil.findFreePort();
            server.bind(tcpPort,udpPort);
            tickCounter.start();
            GameServerController.get().getGameState().prepareWorld(AssetsMaps.MAP);
            GameServerController.get().getGameState().prepareListener();
            System.out.println("Game server is up. TCP port: " + tcpPort + ", UDP port: " + udpPort);
            iAmHost.set(true);
        } catch (IOException e) {
            //TODO handle server start failed
            e.printStackTrace();
        }
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public int getUdpPort() {
        return udpPort;
    }

    public String checkName(String playerDataName) {
        return null;
    }

    public Server getServer() {
        return server;
    }

    public GameState getGameState() {
        return gameState;
    }

    public boolean amIHost() {
        return iAmHost.get();
    }

    public void runTickSynchro(Connection connection) {
        int connectionId = connection.getID();
        final PingPongArray pingPong = GameServerController.get().getGameState()
                .getPingPong().get(connectionId);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            int pingsLeft = 19;
            @Override
            public void run() {
                Ping ping = new Ping(pingsLeft, GameServerController.get().tickCounter.getTick());
                connection.sendUDP(ping);
                pingPong.clear();
                pingPong.addPing(ping);

                if(--pingsLeft < 0){
                    timer.cancel();
                    timer.purge();

                    Timer timer2 = new Timer();
                    timer2.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            GameServerController.get().getGameState()
                                    .syncTick();
                        }
                    },2000);
                };
            }
        }, 0, 50);
    }

    public TickCounter getTickCounter() {
        return tickCounter;
    }
}
