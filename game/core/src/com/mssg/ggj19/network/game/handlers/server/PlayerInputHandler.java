package com.mssg.ggj19.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.objects.PlayerInputEvent;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.PlayerUpdateEvent;

public class PlayerInputHandler implements MessageHandler<PlayerInputEvent> {
    @Override
    public void processMessage(Connection connection, PlayerInputEvent playerInputEvent) {
        GameServerController.get().getGameState().bufferInput(
                connection.getID(), playerInputEvent);

    }
}
