package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.common.ui.ChatView;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.objects.PlayerUpdateEvent;

public class PlayerUpdateHandler implements MessageHandler<PlayerUpdateEvent> {
    @Override
    public void processMessage(Connection connection, PlayerUpdateEvent playerUpdateEvent) {
        GameClientController.get().getGameState().bufferPlayerUpdate(playerUpdateEvent);

    }
}
