package com.mssg.ggj19.network.game;

import com.mssg.ggj19.common.input.InputMonitor;
import com.mssg.ggj19.network.game.objects.PlayerInputEvent;

public class ClientTickCounter extends TickCounter {

    @Override
    protected void doExtraStuff() {
        byte input = InputMonitor.collect();
        PlayerInputEvent playerInputEvent = new PlayerInputEvent();
        playerInputEvent.setControls(input);
        playerInputEvent.setTick(GameClientController.get().getTickCounter().getTick());

        GameClientController.get().getClient()
                .sendUDP(playerInputEvent);
    }
}
