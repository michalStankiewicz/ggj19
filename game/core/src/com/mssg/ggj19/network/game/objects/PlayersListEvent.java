package com.mssg.ggj19.network.game.objects;

import com.mssg.ggj19.game.common.Player;
import com.mssg.ggj19.network.common.Event;

import java.util.Map;

public class PlayersListEvent extends Event {
    private Map<Integer, Player> players;

    public void setPlayers(Map<Integer, Player> players) {
        this.players = players;
    }

    public Map<Integer, Player> getPlayers() {
        return players;
    }
}
