package com.mssg.ggj19.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.common.ui.ChatView;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.ChatEvent;
import com.mssg.ggj19.network.game.objects.PlayerSendMessageEvent;

public class PlayerSendMessageHandler implements MessageHandler<PlayerSendMessageEvent> {
    @Override
    public void processMessage(Connection connection, PlayerSendMessageEvent playerSendMessageEvent) {
        ChatEvent chatEvent = new ChatEvent(playerSendMessageEvent.getPlayer() + ": " + playerSendMessageEvent.getMessage());
        GameServerController.get().getServer()
                .sendToAllTCP(chatEvent);
    }
}
