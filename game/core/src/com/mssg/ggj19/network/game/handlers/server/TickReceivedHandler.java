package com.mssg.ggj19.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.game.GameState;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.StartGameEvent;
import com.mssg.ggj19.network.game.objects.TickReceivedEvent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class TickReceivedHandler implements MessageHandler<TickReceivedEvent> {
    @Override
    public void processMessage(Connection connection, TickReceivedEvent tickReceivedEvent) {
        GameState gameState = GameServerController.get().getGameState();
        if(++gameState.playersReady
                == gameState.getPlayers().size()){

            int teamId = 0;
            ConcurrentMap<Integer, Integer> teams = new ConcurrentHashMap<>();

            StartGameEvent startGameEvent = new StartGameEvent();

            for(int id: gameState.getPlayers().keySet()) {
                int team = teamId;
                if(teamId == 0){
                    teamId = 1;
                } else {
                    teamId = 0;
                }
                teams.put(id,team);
                GameServerController.get().getGameState()
                        .addPlayerPhysics(id, team);
            }

            startGameEvent.setTeams(teams);
            GameServerController.get().getServer()
                    .sendToAllTCP(startGameEvent);
        }
    }
}
