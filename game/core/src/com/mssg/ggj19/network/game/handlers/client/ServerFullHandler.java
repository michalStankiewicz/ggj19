package com.mssg.ggj19.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.mainMenu.menu.MenuStage;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.objects.ServerFullEvent;

public class ServerFullHandler implements MessageHandler<ServerFullEvent> {
    @Override
    public void processMessage(Connection connection, ServerFullEvent serverFullEvent) {
        MenuStage.getInstance().showError("Server full");
    }
}
