package com.mssg.ggj19.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;
import com.mssg.ggj19.assets.AssetsProperties;
import com.mssg.ggj19.game.common.Player;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.objects.ChatEvent;
import com.mssg.ggj19.network.game.objects.GameReadyEvent;
import com.mssg.ggj19.network.game.objects.PlayerJoinConfirmEvent;
import com.mssg.ggj19.network.game.objects.PlayerJoinEvent;
import com.mssg.ggj19.network.game.objects.PlayersListEvent;
import com.mssg.ggj19.network.game.objects.ServerFullEvent;

public class PlayerJoinHandler implements MessageHandler<PlayerJoinEvent> {

    public static final int PLAYERS_NEEDED = Integer.parseInt(AssetsProperties.CONFIG
            .getProperty("multiplayer.players"));

    @Override
    public void processMessage(Connection connection, PlayerJoinEvent playerJoinEvent) {
        if(GameServerController.get().getGameState().getPlayers().size() >= PLAYERS_NEEDED
                || GameServerController.get().getGameState().isGameOn()) {
            ServerFullEvent serverFullEvent = new ServerFullEvent();
            connection.sendTCP(serverFullEvent);
        }


        //add player
        Player player = new Player();
        player.setName(playerJoinEvent.getPlayerName());
        player.setServerId(System.currentTimeMillis());
        GameServerController.get().getGameState().addPlayer(connection.getID(),player);
        //prepare list of all players
        PlayersListEvent playersList = new PlayersListEvent();
        playersList.setPlayers(GameServerController.get().getGameState().getPlayers());
        //send it
        GameServerController.get().getServer().sendToAllTCP(playersList);

        PlayerJoinConfirmEvent playerJoinConfirmEvent = new PlayerJoinConfirmEvent();
        playerJoinConfirmEvent.setConnectionId(connection.getID());
        connection.sendTCP(playerJoinConfirmEvent);

        if(GameServerController.get().getGameState().getPlayers().size() == PLAYERS_NEEDED) {
            GameReadyEvent gameReadyEvent = new GameReadyEvent();
            GameServerController.get().getGameState().setGameOn(true);
            GameServerController.get().getServer()
                    .sendToAllTCP(gameReadyEvent);
        }

        ChatEvent chatEvent = new ChatEvent("Player " + playerJoinEvent.getPlayerName() + " joined");
        GameServerController.get().getServer().sendToAllTCP(chatEvent);
    }
}
