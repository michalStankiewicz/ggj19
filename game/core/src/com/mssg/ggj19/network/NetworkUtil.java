package com.mssg.ggj19.network;

import com.mssg.ggj19.network.common.Event;
import com.mssg.ggj19.network.connector.ConnectorClientController;
import com.mssg.ggj19.network.connector.ConnectorServerController;
import com.mssg.ggj19.network.game.GameClientController;
import com.mssg.ggj19.network.game.GameServerController;

import java.io.IOException;
import java.net.ServerSocket;

public class NetworkUtil {

    public static int findFreePort() {
        int port = -1;
        try {
            ServerSocket socket = new ServerSocket(0);
            port = socket.getLocalPort();
            socket.close();
            return port;
        }
        catch (IOException ioe) {
            return port;
        }
    }

    public static void registerClasses(Class<?>... clazzzzzz) {
        for (int i = 0; i < clazzzzzz.length; i++) {
            ConnectorServerController.get().getConnectorServer().getKryo().register(clazzzzzz[i]);
            ConnectorClientController.get().getConnectorClient().getKryo().register(clazzzzzz[i]);
            GameServerController.get().getServer().getKryo().register(clazzzzzz[i]);
            GameClientController.get().getClient().getKryo().register(clazzzzzz[i]);
        }
    }

    public static <T extends Event> void registerClass(Class<T> clazz) {
        ConnectorServerController.get().getConnectorServer().getKryo().register(clazz);
        ConnectorClientController.get().getConnectorClient().getKryo().register(clazz);
        GameServerController.get().getServer().getKryo().register(clazz);
        GameClientController.get().getClient().getKryo().register(clazz);
    }
}
