package com.mssg.ggj19.network;


import com.mssg.ggj19.game.PropKillEvent;
import com.mssg.ggj19.game.common.Player;
import com.mssg.ggj19.network.common.MessageHandler;
import com.mssg.ggj19.network.game.handlers.client.ItemsTouchedHandler;
import com.mssg.ggj19.network.game.handlers.client.PropKillHandler;
import com.mssg.ggj19.network.game.objects.ItemsTouchedEvent;
import com.mssg.ggj19.network.game.objects.PlayerInputEvent;
import com.mssg.ggj19.network.connector.ConnectorClientController;
import com.mssg.ggj19.network.connector.ConnectorServerController;
import com.mssg.ggj19.network.connector.handlers.client.PlayerInvitationHandler;
import com.mssg.ggj19.network.connector.handlers.client.InvalidPasswordHandler;
import com.mssg.ggj19.network.connector.handlers.server.ConnectionInitHandler;
import com.mssg.ggj19.network.common.SysOutHandler;
import com.mssg.ggj19.network.connector.objects.Ping;
import com.mssg.ggj19.network.connector.objects.PlayerInvitationEvent;
import com.mssg.ggj19.network.connector.objects.InvalidPasswordEvent;
import com.mssg.ggj19.network.connector.objects.ConnectionInitEvent;
import com.mssg.ggj19.network.game.GameServerController;
import com.mssg.ggj19.network.game.handlers.client.ChatHandler;
import com.mssg.ggj19.network.game.handlers.client.GameReadyHandler;
import com.mssg.ggj19.network.game.handlers.client.PingHandler;
import com.mssg.ggj19.network.game.handlers.client.PlayerJoinConfirmHandler;
import com.mssg.ggj19.network.game.handlers.client.PlayerUpdateHandler;
import com.mssg.ggj19.network.game.handlers.client.PlayersListHandler;
import com.mssg.ggj19.network.game.handlers.client.ServerFullHandler;
import com.mssg.ggj19.network.game.handlers.client.StartGameHandler;
import com.mssg.ggj19.network.game.handlers.client.TickUpdateHandler;
import com.mssg.ggj19.network.game.handlers.server.PlayerInputHandler;
import com.mssg.ggj19.network.game.handlers.server.PlayerJoinHandler;
import com.mssg.ggj19.network.game.handlers.server.PlayerLoadedHandler;
import com.mssg.ggj19.network.game.handlers.server.PlayerSendMessageHandler;
import com.mssg.ggj19.network.game.handlers.server.PongHandler;
import com.mssg.ggj19.network.game.handlers.server.TickReceivedHandler;
import com.mssg.ggj19.network.game.objects.ChatEvent;
import com.mssg.ggj19.network.game.objects.GameReadyEvent;
import com.mssg.ggj19.network.game.objects.PlayerJoinConfirmEvent;
import com.mssg.ggj19.network.game.objects.PlayerJoinEvent;
import com.mssg.ggj19.network.game.objects.PlayerLoadedEvent;
import com.mssg.ggj19.network.game.objects.PlayerSendMessageEvent;
import com.mssg.ggj19.network.game.objects.PlayerUpdateEvent;
import com.mssg.ggj19.network.game.objects.PlayersListEvent;
import com.mssg.ggj19.network.game.objects.Pong;
import com.mssg.ggj19.network.game.objects.ServerFullEvent;
import com.mssg.ggj19.network.game.objects.StartGameEvent;
import com.mssg.ggj19.network.game.objects.TickReceivedEvent;
import com.mssg.ggj19.network.game.objects.TickUpdateEvent;

import java.util.BitSet;
import java.util.concurrent.ConcurrentHashMap;

public class NetworkController {

    public static void hostGame(String serverPassword){
        ConnectorServerController.get().startConnectorServer(serverPassword);
        GameServerController.get().startGameServer(serverPassword);
    }

    public static void joinLocal(ConnectionInitEvent connectionInitEvent) {
        ConnectorClientController.get().joinLocal(connectionInitEvent);
    }

    public static void prepareNetworkStuff() {
        prepareInfrastructure();
        prepareHandlers();
        registerAdditionalClasses();
    }

    private static void prepareInfrastructure() {
        ConnectorServerController.get();
        ConnectorClientController.get();
        GameServerController.get();
        GameServerController.get();
    }

    private static void prepareHandlers() {
        MessageHandlers.setDefaultHandler(new SysOutHandler());
//        new NopHandler();

        MessageHandlers.addHandler(ConnectionInitEvent.class, new ConnectionInitHandler());
        MessageHandlers.addHandler(InvalidPasswordEvent.class, new InvalidPasswordHandler());
        MessageHandlers.addHandler(PlayerInvitationEvent.class, new PlayerInvitationHandler());
        MessageHandlers.addHandler(PlayerJoinEvent.class, new PlayerJoinHandler());
        MessageHandlers.addHandler(PlayersListEvent.class, new PlayersListHandler());
        MessageHandlers.addHandler(ChatEvent.class, new ChatHandler());
        MessageHandlers.addHandler(PlayerSendMessageEvent.class, new PlayerSendMessageHandler());
        MessageHandlers.addHandler(PlayerJoinConfirmEvent.class, new PlayerJoinConfirmHandler());
        MessageHandlers.addHandler(ServerFullEvent.class, new ServerFullHandler());
        MessageHandlers.addHandler(GameReadyEvent.class, new GameReadyHandler());
        MessageHandlers.addHandler(PlayerLoadedEvent.class, new PlayerLoadedHandler());
        MessageHandlers.addHandler(Ping.class, new PingHandler());
        MessageHandlers.addHandler(Pong.class, new PongHandler());
        MessageHandlers.addHandler(TickUpdateEvent.class, new TickUpdateHandler());
        MessageHandlers.addHandler(TickReceivedEvent.class, new TickReceivedHandler());
        MessageHandlers.addHandler(StartGameEvent.class, new StartGameHandler());
        MessageHandlers.addHandler(PlayerInputEvent.class, new PlayerInputHandler());
        MessageHandlers.addHandler(PlayerUpdateEvent.class, new PlayerUpdateHandler());
        MessageHandlers.addHandler(ItemsTouchedEvent.class, new ItemsTouchedHandler());
        MessageHandlers.addHandler(PropKillEvent.class, new PropKillHandler());

    }

    private static void registerAdditionalClasses() {
        NetworkUtil.registerClasses(ConcurrentHashMap.class,
                Player.class,
                BitSet.class);
    }
}
