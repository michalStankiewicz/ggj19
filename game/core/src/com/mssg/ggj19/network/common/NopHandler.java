package com.mssg.ggj19.network.common;

import com.esotericsoftware.kryonet.Connection;

public class NopHandler implements MessageHandler<Object> {

    @Override
    public void processMessage(Connection connection, Object o) {
        //nop
    }
}
