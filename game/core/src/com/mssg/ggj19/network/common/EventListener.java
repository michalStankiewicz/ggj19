package com.mssg.ggj19.network.common;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.mssg.ggj19.network.MessageHandlers;

public class EventListener extends Listener {
    private static final EventListener THIS = new EventListener();

    public static final EventListener get(){
        return THIS;
    }

    private EventListener() {
    }

    @Override
    public void received(Connection connection, Object object) {
//        try {
                MessageHandlers.getFor(object.getClass())
                        .handleMessage(connection, object);
//        }
//        catch (Exception e) {
//            int k = 2;
//        }
    }
}
