package com.mssg.ggj19.network.common;

import com.esotericsoftware.kryonet.Connection;

public interface MessageHandler<T> {

    default void handleMessage(Connection connection, Object object){
        T t = (T) object;
        processMessage(connection, t);
    }

    void processMessage(Connection connection, T t);
}
