package com.mssg.ggj19.network;

import com.mssg.ggj19.network.common.Event;
import com.mssg.ggj19.network.common.MessageHandler;

import java.util.HashMap;
import java.util.Map;

public class MessageHandlers {

    private static final Map<Class<?>, MessageHandler> MESSAGE_HANDLERS = new HashMap<>();
    private static MessageHandler defaultHandler;

    public static MessageHandler getFor(Class<?> clazz) {
        MessageHandler messageHandler = MESSAGE_HANDLERS.get(clazz);
        if (null != messageHandler){
            return messageHandler;
        } else {
            return defaultHandler;
        }
    }

    public static <T extends Event> void addHandler(Class<T> clazz,
                                                    MessageHandler<T> handler) {
        MESSAGE_HANDLERS.put(clazz, handler);
        NetworkUtil.registerClass(clazz);
    }

    public static void setDefaultHandler(MessageHandler handler) {
        defaultHandler = handler;
    }
}
