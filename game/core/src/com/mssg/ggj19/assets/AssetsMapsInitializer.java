package com.mssg.ggj19.assets;

import com.badlogic.gdx.maps.tiled.TiledMap;

public class AssetsMapsInitializer extends AssetsLoader{

    public static final String MAP_FILE = "levels/house.tmx";

    @Override
    protected void loadAssets() {
        assetManager.load(MAP_FILE, TiledMap.class);
    }

    @Override
    protected void initAssets() {
        AssetsMaps.MAP = assetManager.get(MAP_FILE, TiledMap.class);
    }

    @Override
    protected void disposeAssets() {

    }
}
