package com.mssg.ggj19.assets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class AssetGraphicsLoader extends AssetsLoader {
    private static final String GHOST_ATLAS_SRC = "gfx/ghost/ghost_animation.atlas";
    private static final String EXORCIST_ATLAS_SRC = "gfx/exorcist/exorcist.atlas";
    private static final String GHOST_EYES_SRC = "gfx/ghost/ghost_eyes.png";
    private static final String EXORCIST_ATTACK_SRC = "gfx/exorcist/exorcist_attack.png";

    private static final String CANDLE_SRC = "gfx/items/candle.png";
    private static final String DISHES_SRC = "gfx/items/dishes.png";
    private static final String CHAIR_SRC = "gfx/items/chair.png";
    private static final String BOOK_SRC = "gfx/items/book.png";
    private static final String BURET_SRC = "gfx/items/buret.png";
    private static final String COFFE_SRC = "gfx/items/coffe.png";
    private static final String DISH_SRC = "gfx/items/dish.png";
    private static final String OPEN_BOOK_SRC = "gfx/items/open_book.png";
    private static final String PHYLACTERY_SRC = "gfx/items/phylactery.png";
    private static final String PLATE_SRC = "gfx/items/plate.png";
    private static final String UPSIDE_DOWN_BOOK_SRC = "gfx/items/upside_down_book.png";
    private static final String WINE_SRC = "gfx/items/wine.png";

    private static final String BG_SRC = "gfx/startowy.jpg";

    @Override
    protected void loadAssets() {
        assetManager.load(GHOST_ATLAS_SRC, TextureAtlas.class);
        assetManager.load(EXORCIST_ATLAS_SRC, TextureAtlas.class);
        assetManager.load(GHOST_EYES_SRC, Texture.class);
        assetManager.load(EXORCIST_ATTACK_SRC, Texture.class);
        assetManager.load(CANDLE_SRC, Texture.class);
        assetManager.load(DISHES_SRC, Texture.class);
        assetManager.load(CHAIR_SRC, Texture.class);
        assetManager.load(BOOK_SRC, Texture.class);
        assetManager.load(BURET_SRC, Texture.class);
        assetManager.load(COFFE_SRC, Texture.class);
        assetManager.load(DISH_SRC, Texture.class);
        assetManager.load(OPEN_BOOK_SRC, Texture.class);
        assetManager.load(PHYLACTERY_SRC, Texture.class);
        assetManager.load(PLATE_SRC, Texture.class);
        assetManager.load(UPSIDE_DOWN_BOOK_SRC, Texture.class);
        assetManager.load(WINE_SRC, Texture.class);
        assetManager.load(BG_SRC, Texture.class);
    }

    @Override
    protected void initAssets() {
        AssetGraphics.GHOST_ATLAS = assetManager
                .get(GHOST_ATLAS_SRC, TextureAtlas.class);
        AssetGraphics.EXORCIST_ATLAS = assetManager
                .get(EXORCIST_ATLAS_SRC, TextureAtlas.class);
        AssetGraphics.GHOST_EYES = new Sprite(
                assetManager.get(GHOST_EYES_SRC, Texture.class));
        AssetGraphics.EXORCIST_ATTACK = new Sprite(
                assetManager.get(EXORCIST_ATTACK_SRC, Texture.class));
        AssetGraphics.ITEM_CANDLE = new Sprite(
                assetManager.get(CANDLE_SRC, Texture.class));
        AssetGraphics.ITEM_DISHES = new Sprite(
                assetManager.get(DISHES_SRC, Texture.class));
        AssetGraphics.ITEM_CHAIR = new Sprite(
                assetManager.get(CHAIR_SRC, Texture.class));
        AssetGraphics.ITEM_BOOK = new Sprite(
                assetManager.get(BOOK_SRC, Texture.class));
        AssetGraphics.ITEM_BURET = new Sprite(
                assetManager.get(BURET_SRC, Texture.class));
        AssetGraphics.ITEM_COFFE = new Sprite(
                assetManager.get(COFFE_SRC, Texture.class));
        AssetGraphics.ITEM_DISH = new Sprite(
                assetManager.get(DISH_SRC, Texture.class));
        AssetGraphics.ITEM_OPEN_BOOK = new Sprite(
                assetManager.get(OPEN_BOOK_SRC, Texture.class));
        AssetGraphics.ITEM_PHYLACTERY = new Sprite(
                assetManager.get(PHYLACTERY_SRC, Texture.class));
        AssetGraphics.ITEM_PLATE = new Sprite(
                assetManager.get(PLATE_SRC, Texture.class));
        AssetGraphics.ITEM_UPSIDE_DOWN_BOOK = new Sprite(
                assetManager.get(UPSIDE_DOWN_BOOK_SRC, Texture.class));
        AssetGraphics.ITEM_WINE = new Sprite(
                assetManager.get(WINE_SRC, Texture.class));
        AssetGraphics.BACKGROUND = new Sprite(
                assetManager.get(BG_SRC, Texture.class));
    }

    @Override
    protected void disposeAssets() {

    }
}
