package com.mssg.ggj19.assets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

class AssetsUIInitializer extends AssetsLoader{

    public static final String SHADE_ATLAS_SRC = "gfx/ui/styles/shade/uiskin.atlas";
    public static final String SHADE_SKIN_SRC = "gfx/ui/styles/shade/uiskin.json";

    @Override
    protected void loadAssets() {
        assetManager.load(SHADE_ATLAS_SRC, TextureAtlas.class);
        assetManager.load(SHADE_SKIN_SRC, Skin.class);
    }

    @Override
    protected void initAssets() {
        AssetsUI.SHADE_ATLAS = assetManager.get(SHADE_ATLAS_SRC, TextureAtlas.class);
        AssetsUI.SHADE_SKIN = assetManager.get(SHADE_SKIN_SRC, Skin.class);
    }

    @Override
    protected void disposeAssets() {
        AssetsUI.SHADE_ATLAS.dispose();
        AssetsUI.SHADE_SKIN.dispose();
    }
}
