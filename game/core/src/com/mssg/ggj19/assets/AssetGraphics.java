package com.mssg.ggj19.assets;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class AssetGraphics {
    public static Sprite GHOST_EYES;
    public static TextureAtlas GHOST_ATLAS;
    public static TextureAtlas EXORCIST_ATLAS;
    public static Sprite ITEM_CANDLE;
    public static Sprite ITEM_DISHES;
    public static Sprite ITEM_CHAIR;
    public static Sprite ITEM_BOOK;
    public static Sprite ITEM_BURET;
    public static Sprite ITEM_COFFE;
    public static Sprite ITEM_DISH;
    public static Sprite ITEM_OPEN_BOOK;
    public static Sprite ITEM_PHYLACTERY;
    public static Sprite ITEM_PLATE;
    public static Sprite ITEM_UPSIDE_DOWN_BOOK;
    public static Sprite ITEM_WINE;
    public static Sprite EXORCIST_ATTACK;
    public static Sprite BACKGROUND;
}
