package com.mssg.ggj19.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Disposable;

import java.util.HashMap;
import java.util.Map;

public abstract class AssetsLoader implements Disposable {
    private static Map<Class<?>, AssetsLoader> assetLoaders;
    static final AssetManager assetManager = new AssetManager();

    static {
        assetManager.setLoader(TiledMap.class,
                new TmxMapLoader(new InternalFileHandleResolver()));
    }

    @Override
    public final void dispose(){
        disposeAssets();
        assetManager.dispose();
    }

    protected abstract void loadAssets();
    protected abstract void initAssets();
    protected abstract void disposeAssets();

    public static void loadAllAssets() {
        assetLoaders = new HashMap<Class<?>, AssetsLoader>();
        register(AssetsUIInitializer.class, new AssetsUIInitializer());
        register(AssetGraphicsLoader.class, new AssetGraphicsLoader());
        register(AssetsI18NInitializer.class, new AssetsI18NInitializer());
        register(AssetsMaps.class, new AssetsMapsInitializer());
    }

    private static void register(Class<?> assetsLoaderClass, AssetsLoader assetsLoader) {
        assetLoaders.put(assetsLoaderClass, assetsLoader);
        assetsLoader.loadAssets();
    }

    public static void disposeAllAssets() {
        for (Map.Entry<Class<?>, AssetsLoader> assetsLoaderEntry:
            assetLoaders.entrySet()){
            assetsLoaderEntry.getValue().dispose();
        }
    }

    public static boolean isLoadingFinished() {
        return assetManager.update();
    }

    public static int getProgressPrc() {
        int progress = (int) (assetManager.getProgress() * 100);
        return progress;
    }

    public static void initializeAssets() {
        for (Map.Entry<Class<?>, AssetsLoader> assetsLoaderEntry:
                assetLoaders.entrySet()){
            assetsLoaderEntry.getValue().initAssets();
        }
    }
}
