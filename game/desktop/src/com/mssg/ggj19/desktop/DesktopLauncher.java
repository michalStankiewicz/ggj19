package com.mssg.ggj19.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mssg.ggj19.GGJ19Main;
import com.mssg.ggj19.assets.AssetsProperties;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

public class DesktopLauncher {
	public static final String GAME_CONFIG_FILE = "config.properties";

	public static void main (String[] arg) {
		loadConfig();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = Integer.parseInt(AssetsProperties.CONFIG.getProperty("resolution.width"));
		config.height = Integer.parseInt(AssetsProperties.CONFIG.getProperty("resolution.height"));
		config.fullscreen = Boolean.parseBoolean(AssetsProperties.CONFIG.getProperty("fullscreenMode"));
		config.forceExit = true;

		new LwjglApplication(new GGJ19Main(), config);
	}

	private static void loadConfig() {
		AssetsProperties.CONFIG = new Properties();
		try {
			AssetsProperties.CONFIG.load(new FileInputStream(GAME_CONFIG_FILE));
		} catch (IOException e) {
			e.printStackTrace();
		}
		AssetsProperties.CONFIG.put("runtimePath", Paths.get(".").toAbsolutePath().normalize().toString());
	}
}
